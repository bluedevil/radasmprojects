//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by PastilleMaker.rc
//
#define IDD_PASTILLEMAKER               101
#define IDD_MAIN                        101
#define IDI_MAIN                        103
#define IDC_IFILE                       1000
#define IDC_INPUTFILEPATH               1000
#define IDC_OFILE                       1001
#define IDC_OUTPUTFILEPATH              1001
#define IDC_IBROWSE                     1002
#define IDC_BROWSEINPUTFILE             1002
#define IDC_OBROWSE                     1003
#define IDC_BROWSEOUTPUTFILE            1003
#define IDC_DEFFILENAME                 1004
#define IDC_DEFFILEPATH                 1005
#define IDC_DEFFILEDIR                  1005
#define IDC_TITLE                       1006
#define IDC_INFO                        1007
#define IDC_MAKE                        1008
#define IDC_EXIT                        1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
