/* Pastille Maker - Macros
 * =============================================================================
 * Author : Dean/Canterwood <charloweb@hotmail.com>
 * Website: http://kickme.to/apz
 * IDE    : Microsoft Visual C++ 6.0
 * =============================================================================
 * 08.26.2003 */

#define STRING(data) #data
#define INT2STR(n) TEXT(STRING(##n##))
