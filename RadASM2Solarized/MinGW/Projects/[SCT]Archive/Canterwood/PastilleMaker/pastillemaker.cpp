/* Pastille Maker
 * =============================================================================
 * Author : Dean/Canterwood <charloweb@hotmail.com>
 * Website: http://kickme.to/apz
 * IDE    : Microsoft Visual C++ 6.0
 * =============================================================================
 * v0.3 [08.26.2003] */

#include "pastillemaker.h"

BOOL CALLBACK DialogProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static TCHAR lpPatchName[MAX_PATH] = TEXT("");
    TCHAR lpInputFilePath[MAX_INPUTFILEPATH] = TEXT(""), lpOutputFilePath[MAX_OUTPUTFILEPATH] = TEXT("");
	DWORD nDiffCount;
	LPDWORD lpOffsets;
	LPBYTE lpOldValues, lpNewValues;
	TCHAR lpLastPatchPath[MAX_PATH] = TEXT("");
	HANDLE hPatchUpdate;
	CHAR lpDefFileDir[MAX_DEFFILEDIR] = "", lpDefFilename[MAX_DEFFILENAME] = "", lpDefFilePath[MAX_PATH] = "";
	CHAR lpOpenFilter[MAX_PATH] = "";
	DWORD nOpenFilterLength;
	DWORD nInputFileSize, nOutputFileSize;
	DWORD nInputFileChecksum, nOutputFileChecksum;
	CHAR lpTitle[MAX_TITLE] = "", lpInfo[MAX_INFO] = "";

	switch(uMsg)
	{
		case WM_INITDIALOG:
			SendMessage(hWnd, WM_SETICON, ICON_BIG, (LPARAM)LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_MAIN)));
			SetWindowText(hWnd, TEXT("PastilleMaker ") PASTILLEMAKERVERSIONEX TEXT(" by Canterwood"));
			SetLimitText(hWnd, IDC_INPUTFILEPATH, MAX_INPUTFILEPATH);
			SetLimitText(hWnd, IDC_OUTPUTFILEPATH, MAX_OUTPUTFILEPATH);
			SetLimitText(hWnd, IDC_DEFFILEDIR, MAX_DEFFILEDIR);
			SetLimitText(hWnd, IDC_DEFFILENAME, MAX_DEFFILENAME);
			SetLimitText(hWnd, IDC_TITLE, MAX_TITLE);
			SetLimitText(hWnd, IDC_INFO, MAX_INFO);
		break;
		case WM_COMMAND:
			switch(wParam)
			{
				case IDC_BROWSEINPUTFILE:
					BrowseInputFile(hWnd, lpPatchName);
					CharLower(lpPatchName);
					lstrcat(lpPatchName, TEXT("."));
					lstrcat(lpPatchName, LASTPATCHNAME);
				break;
				case IDC_BROWSEOUTPUTFILE:
					BrowseOutputFile(hWnd);
				break;
				case IDC_MAKE:
					EnableWindow(GetDlgItem(hWnd, IDC_MAKE), FALSE);

					if(IsTextFieldOK(hWnd, IDC_INPUTFILEPATH) && IsTextFieldOK(hWnd, IDC_OUTPUTFILEPATH) &&
					   IsTextFieldOK(hWnd, IDC_DEFFILEDIR) && IsTextFieldOK(hWnd, IDC_DEFFILENAME) &&
					   IsTextFieldOK(hWnd, IDC_TITLE) && IsTextFieldOK(hWnd, IDC_INFO))
					{
						GetDlgItemText(hWnd, IDC_INPUTFILEPATH, lpInputFilePath, MAX_INPUTFILEPATH);
						GetDlgItemText(hWnd, IDC_OUTPUTFILEPATH, lpOutputFilePath, MAX_OUTPUTFILEPATH);

						nDiffCount = GetFileDiffN(lpInputFilePath, lpOutputFilePath);

						if(nDiffCount > 0)
						{
							lpOffsets = (LPDWORD)GlobalAlloc(GPTR, nDiffCount * sizeof(DWORD));
							lpNewValues = (LPBYTE)GlobalAlloc(GPTR, nDiffCount);

							if(lpOffsets != NULL && lpNewValues != NULL)
							{
								if(GetFileDiff(lpInputFilePath, lpOutputFilePath, lpOffsets, NULL, lpNewValues, nDiffCount) == nDiffCount)
								{
									if(CopyPatchTemplate(lpLastPatchPath))
									{
										hPatchUpdate = BeginUpdateResource(LASTPATCHNAME, FALSE);

										if(hPatchUpdate != NULL)
										{
											GetDlgItemTextA(hWnd, IDC_DEFFILEDIR, lpDefFileDir, MAX_DEFFILEDIR);
											GetDlgItemTextA(hWnd, IDC_DEFFILENAME, lpDefFilename, MAX_DEFFILENAME);
											lstrcpyA(lpDefFilePath, lpDefFileDir);
											lstrcatA(lpDefFilePath, lpDefFilename);

											SaveRCData(hPatchUpdate, TEXT("TARGETPATH"), lpDefFilePath, lstrlenA(lpDefFilePath) + 1);

											nOpenFilterLength = MakeOpenFilter(lpOpenFilter, lpDefFilename);

											SaveRCData(hPatchUpdate, TEXT("OPENFILTER"), lpOpenFilter, nOpenFilterLength);

											nInputFileSize = GetFileSizeFromName(lpInputFilePath);

											SaveRCData(hPatchUpdate, TEXT("TARGETSIZE"), &nInputFileSize, sizeof(nInputFileSize));

											nInputFileChecksum = GetFileChecksum(lpInputFilePath);

											SaveRCData(hPatchUpdate, TEXT("TARGETCHECKSUM"), &nInputFileChecksum, sizeof(nInputFileChecksum));

											SaveRCData(hPatchUpdate, TEXT("CHANGEOFFSETS"), lpOffsets, nDiffCount * sizeof(DWORD));

											SaveRCData(hPatchUpdate, TEXT("NEWBYTES"), lpNewValues, nDiffCount);

											nOutputFileSize = GetFileSizeFromName(lpOutputFilePath);

											SaveRCData(hPatchUpdate, TEXT("TARGETNEWSIZE"), &nOutputFileSize, sizeof(nOutputFileSize));

											nOutputFileChecksum = GetFileChecksum(lpOutputFilePath);

											SaveRCData(hPatchUpdate, TEXT("TARGETNEWCHECKSUM"), &nOutputFileChecksum, sizeof(nOutputFileChecksum));

											GetDlgItemTextA(hWnd, IDC_TITLE, lpTitle, MAX_TITLE);

											SaveRCData(hPatchUpdate, TEXT("TITLE"), lpTitle, lstrlenA(lpTitle) + 1);

											GetDlgItemTextA(hWnd, IDC_INFO, lpInfo, MAX_INFO);

											SaveRCData(hPatchUpdate, TEXT("INFO"), lpInfo, lstrlenA(lpInfo) + 1);

											if(EndUpdateResource(hPatchUpdate, FALSE))
											{
												if(SaveFile(hWnd, lpPatchName, lpLastPatchPath))
													MessageBox(hWnd, TEXT("Patch successfully saved!"), TEXT("Information"), MB_ICONINFORMATION);
											}
											else
												ShowErrorMsg(hWnd, TEXT("Cannot update the patch."));
										}
										else
											ShowErrorMsg(hWnd, TEXT("Cannot open the patch copy for update."));
									}
									else
										ShowErrorMsg(hWnd, TEXT("Cannot copy the patch template (") PATCHTEMPLATENAME TEXT(")."));
								}
								else
									ShowErrorMsg(hWnd, TEXT("An error has occured when trying to read the files."));

								GlobalFree(lpOffsets);
								GlobalFree(lpNewValues);
							}
							else
								ShowErrorMsg(hWnd, TEXT("Cannot allocate enough memory to store the patch information."));
						}
						else
							ShowErrorMsg(hWnd, TEXT("The input file and the output file are identical or an error has occured when trying to read them."));
					}
					else
						ShowErrorMsg(hWnd, TEXT("You must fill all fields."));

					EnableWindow(GetDlgItem(hWnd, IDC_MAKE), TRUE);
				break;
				case IDC_EXIT:
					SendMessage(hWnd, WM_CLOSE, 0, 0);
				break;
			}
		break;
		case WM_CLOSE:
			EndDialog(hWnd, 0);
		break;
		default:
			return FALSE;
	}

	return TRUE;
}

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, INT nCmdShow)
{
	return DialogBox(hInstance, MAKEINTRESOURCE(IDD_MAIN), NULL, DialogProc);
}