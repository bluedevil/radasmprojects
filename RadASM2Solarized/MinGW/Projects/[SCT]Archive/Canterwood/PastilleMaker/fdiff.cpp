/* fDiff library
 * =============================================================================
 * Author : Dean/Canterwood <charloweb@hotmail.com>
 * Website: http://kickme.to/apz
 * IDE    : Microsoft Visual C++ 6.0
 * =============================================================================
 * v1.0 [08.23.2003] */

#include <windows.h>

#include "fdiff.h"

 /* GetFileDiffN
 * -----------------------------------------------------------------------------
 * The GetFileDiffN function returns the number of differences between two
 * files, in terms of offsets/values only. Other characteristics as the size,
 * the attributes of the files are ignored and must be taken in account
 * separately.
 * -----------------------------------------------------------------------------
 * Parameters   : - lpInputFilename : the filename of the input/original file;
 *                - lpOutputFilename: the filename of the ouput/modified file;
 * -----------------------------------------------------------------------------
 * Return values: - if the function succeeds, the return value is the number of
 *                  differences found between the two files.
 *                - if the function fails, or if the files are equal, the return
 *                  value is 0.
 */
DWORD GetFileDiffN(LPCTSTR lpInputFilename, LPCTSTR lpOutputFilename)
{
	HANDLE hInputFile, hOutputFile;
	DWORD dwInputFileSize, dwOutputFileSize;
	HANDLE hInputFileMap, hOutputFileMap;
	LPVOID lpInputFile, lpOutputFile;
	DWORD i;
	BYTE nOldValue, nNewValue;
	DWORD dwCount = 0;

	hInputFile = CreateFile(lpInputFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if(hInputFile != INVALID_HANDLE_VALUE)
	{
		dwInputFileSize = GetFileSize(hInputFile, NULL);

		if(dwInputFileSize != 0xFFFFFFFF && dwInputFileSize != 0)
		{
			hOutputFile = CreateFile(lpOutputFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

			if(hOutputFile != INVALID_HANDLE_VALUE)
			{
				dwOutputFileSize = GetFileSize(hOutputFile, NULL);

				if(dwOutputFileSize != 0xFFFFFFFF && dwOutputFileSize != 0)
				{
					hInputFileMap = CreateFileMapping(hInputFile, NULL, PAGE_READONLY, 0, 0, NULL);

					if(hInputFileMap != NULL)
					{
						hOutputFileMap = CreateFileMapping(hOutputFile, NULL, PAGE_READONLY, 0, 0, NULL);

						if(hOutputFileMap != NULL)
						{
							lpInputFile = MapViewOfFile(hInputFileMap, FILE_MAP_READ, 0, 0, 0);

							if(lpInputFile != NULL)
							{
								lpOutputFile = MapViewOfFile(hOutputFileMap, FILE_MAP_READ, 0, 0, 0);

								if(lpOutputFile != NULL)
								{
									for(i = 0; i < dwOutputFileSize; i++)
									{
										nOldValue = ReadByte((LPBYTE)lpInputFile, i, dwInputFileSize);
										nNewValue = ReadByte((LPBYTE)lpOutputFile, i, dwOutputFileSize);

										if(nOldValue != nNewValue)
											dwCount++;
									}

									UnmapViewOfFile(lpOutputFile);
								}
								else
									return 0;

								UnmapViewOfFile(lpInputFile);
							}
							else
								return 0;

							CloseHandle(hOutputFileMap);
						}
						else
							return 0;

						CloseHandle(hInputFileMap);
					}
					else
						return 0;
				}
				else
					return 0;

				CloseHandle(hOutputFile);
			}
			else
				return 0;
		}
		else
			return 0;

		CloseHandle(hInputFile);
	}
	else
		return 0;

	return dwCount;
}

/* GetFileDiff
 * -----------------------------------------------------------------------------
 * The GetFileDiff function reports the differences between two files, in terms
 * of offsets/values only. Other characteristics as the size, the attributes of
 * the files are ignored and must be taken in account separately.
 * -----------------------------------------------------------------------------
 * Parameters   : - lpInputFilename : the filename of the input/original file;
 *                - lpOutputFilename: the filename of the ouput/modified file;
 *                - lpOffsets       : a pointer to a DWORD array that will store
 *                                    the offsets values;
 *                - lpOldValues     : a pointer to a BYTE array that will store
 *                                    the old values. This member can be NULL;
 *                - lpNewValues     : a pointer to a BYTE array that will store
 *                                    the new values;
 *                - dwBufSize       : the size of the arrays previously
 *                                    declared.
 *
 * IMPORTANT    : the three arrays lpOffsets, lpOldValues and lpNewValues must
 * have the same size. If not, the function may have a strange behavior, and
 * some serious errors can occur.
 * -----------------------------------------------------------------------------
 * Return values: - if the function succeeds, the return value is the number of
 *                  differences found between the two files, adjusted according
 *                  to the size of the memory buffer;
 *                - if the function fails, or if the files are equal, the return
 *                  value is 0.
 */
DWORD GetFileDiff(LPCTSTR lpInputFilename, LPCTSTR lpOutputFilename, LPDWORD lpOffsets, LPBYTE lpOldValues, LPBYTE lpNewValues, DWORD dwBufSize)
{
	HANDLE hInputFile, hOutputFile;
	DWORD dwInputFileSize, dwOutputFileSize;
	HANDLE hInputFileMap, hOutputFileMap;
	LPVOID lpInputFile, lpOutputFile;
	DWORD i;
	BYTE nOldValue, nNewValue;
	DWORD dwCount = 0;

	hInputFile = CreateFile(lpInputFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if(hInputFile != INVALID_HANDLE_VALUE)
	{
		dwInputFileSize = GetFileSize(hInputFile, NULL);

		if(dwInputFileSize != 0xFFFFFFFF && dwInputFileSize != 0)
		{
			hOutputFile = CreateFile(lpOutputFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

			if(hOutputFile != INVALID_HANDLE_VALUE)
			{
				dwOutputFileSize = GetFileSize(hOutputFile, NULL);

				if(dwOutputFileSize != 0xFFFFFFFF && dwOutputFileSize != 0)
				{
					hInputFileMap = CreateFileMapping(hInputFile, NULL, PAGE_READONLY, 0, 0, NULL);

					if(hInputFileMap != NULL)
					{
						hOutputFileMap = CreateFileMapping(hOutputFile, NULL, PAGE_READONLY, 0, 0, NULL);

						if(hOutputFileMap != NULL)
						{
							lpInputFile = MapViewOfFile(hInputFileMap, FILE_MAP_READ, 0, 0, 0);

							if(lpInputFile != NULL)
							{
								lpOutputFile = MapViewOfFile(hOutputFileMap, FILE_MAP_READ, 0, 0, 0);

								if(lpOutputFile != NULL)
								{
									for(i = 0; i < dwOutputFileSize; i++)
									{
										nOldValue = ReadByte((LPBYTE)lpInputFile, i, dwInputFileSize);
										nNewValue = ReadByte((LPBYTE)lpOutputFile, i, dwOutputFileSize);

										if(dwCount < dwBufSize && nOldValue != nNewValue)
										{
											lpOffsets[dwCount] = i;

											if(lpOldValues != NULL)
												lpOldValues[dwCount] = nOldValue;

											lpNewValues[dwCount] = nNewValue;

											dwCount++;
										}
									}

									UnmapViewOfFile(lpOutputFile);
								}
								else
									return 0;

								UnmapViewOfFile(lpInputFile);
							}
							else
								return 0;

							CloseHandle(hOutputFileMap);
						}
						else
							return 0;

						CloseHandle(hInputFileMap);
					}
					else
						return 0;
				}
				else
					return 0;

				CloseHandle(hOutputFile);
			}
			else
				return 0;
		}
		else
			return 0;

		CloseHandle(hInputFile);
	}
	else
		return 0;

	return dwCount;
}

/* ReadByte
 * -----------------------------------------------------------------------------
 * The ReadByte reads one byte in a buffer and return it.
 * -----------------------------------------------------------------------------
 * Parameters   : - lpBuf    : the buffer, it must be casted to LPBYTE;
 *                - dwIndex  : the index, where to read the byte;
 *                - dwBufSize: the size of the buffer.
 * -----------------------------------------------------------------------------
 * Return values: - if the function succeeds, the return value is the byte of
 *                  the buffer stored at the dwIndex position;
 *                - if the function fails, the return value is 0.
 */
BYTE ReadByte(LPBYTE lpBuf, DWORD dwIndex, DWORD dwBufSize)
{
	if(dwIndex < dwBufSize)
		return lpBuf[dwIndex];
	else
		return 0;
}