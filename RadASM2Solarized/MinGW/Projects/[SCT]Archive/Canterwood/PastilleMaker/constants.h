/* Pastille Maker - Constants
 * =============================================================================
 * Author : Dean/Canterwood <charloweb@hotmail.com>
 * Website: http://kickme.to/apz
 * IDE    : Microsoft Visual C++ 6.0
 * =============================================================================
 * 08.26.2003 */

#include "macros.h"

#define PASTILLEMAKERMAJORVERSION 0
#define PASTILLEMAKERMINORVERSION 3

#define PASTILLEMAKERVERSION      INT2STR(PASTILLEMAKERMAJORVERSION) TEXT(".") INT2STR(PASTILLEMAKERMINORVERSION)
#define PASTILLEMAKERVERSIONEX    PASTILLEMAKERVERSION TEXT(" [") TEXT( __DATE__ ) TEXT("]")

#define MAX_INPUTFILEPATH         MAX_PATH
#define MAX_OUTPUTFILEPATH        MAX_INPUTFILEPATH
#define MAX_DEFFILEDIR            MAX_PATH
#define MAX_DEFFILENAME           MAX_PATH
#define MAX_TITLE                 0x80
#define MAX_INFO                  0x200

#define PATCHTEMPLATENAME         TEXT("patch.template.dat")
#define LASTPATCHNAME             TEXT("patch.exe")
