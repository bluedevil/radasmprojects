/* fDiff library - Prototypes
 * =============================================================================
 * Author : Dean/Canterwood <charloweb@hotmail.com>
 * Website: http://kickme.to/apz
 * IDE    : Microsoft Visual C++ 6.0
 * =============================================================================
 * 08.21.2003 */

DWORD GetFileDiffN(LPCTSTR lpInputFilename, LPCTSTR lpOutputFilename);
DWORD GetFileDiff(LPCTSTR lpInputFilename, LPCTSTR lpOutputFilename, LPDWORD lpOffsets, LPBYTE lpOldValues, LPBYTE lpNewValues, DWORD dwBufSize);
BYTE ReadByte(LPBYTE lpBuf, DWORD dwIndex, DWORD dwBufSize);