PastilleMaker - ReadMe
================================================================================
Author : Dean/Canterwood <charloweb@hotmail.com>
Website: http://kickme.to/charloweb
================================================================================
08.26.2003

That's the source code of PastilleMaker. It is a Visual C++ 6.0 project, but you
can compile it with any C++ compiler under Windows.
You can freely modify it and take some part sof code, but don't forget to write
anywhere the author's name!

The compiled file is in the "out" folder. If you are under Windows 9x / Me, you
must copy the file "unicows.dll" (located in the root) in the same directory as
pastillemaker.exe.