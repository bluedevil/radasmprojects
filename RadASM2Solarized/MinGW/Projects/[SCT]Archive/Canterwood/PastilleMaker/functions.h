/* Pastille Maker - Functions - Prototypes
 * =============================================================================
 * Author : Dean/Canterwood <charloweb@hotmail.com>
 * Website: http://kickme.to/apz
 * IDE    : Microsoft Visual C++ 6.0
 * =============================================================================
 * 08.26.2003 */

VOID BrowseInputFile(HWND hWnd, LPTSTR lpPatchName);
VOID BrowseOutputFile(HWND hWnd);
BOOL CopyPatchTemplate(LPTSTR lpLastPatchPath);
OPENFILENAME FillFilePathField(HWND hWnd, INT iFilePathField);
DWORD GetFileChecksum(LPTSTR lpFilename);
DWORD GetFileSizeFromName(LPCTSTR lpFilename);
BOOL IsTextFieldOK(HWND hWnd, INT iTextField);
DWORD MakeOpenFilter(LPSTR lpOpenFilter, LPCSTR lpFilename);
VOID SetLimitText(HWND hWnd, INT iTextField, SHORT nBufSize);
BOOL SaveFile(HWND hWnd, LPTSTR lpSaveFilename, LPCTSTR lpFilename);
BOOL SaveRCData(HANDLE hUpdate, LPCTSTR lpName, LPVOID lpData, DWORD nDataSize);
VOID ShowErrorMsg(HWND hWnd, LPCTSTR lpErrorMsg);