#include <windows.h>
#include <winbase.h>

void displayPrcessorInfo( SYSTEM_INFO &stInfo ) 
{
	switch( stInfo.wProcessorArchitecture )
	{
	case PROCESSOR_ARCHITECTURE_INTEL:
		printf( "Processor Architecture: Intel x86\n");
		break;
	case PROCESSOR_ARCHITECTURE_IA64:
		printf( "Processor Type: Intel x64\n");
		break;
	case PROCESSOR_ARCHITECTURE_AMD64:
		printf( "Processor Type: AMD 64\n");
		break;
	default:
		printf( "Unknown processor architecture\n");
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	SYSTEM_INFO stInfo;
	GetSystemInfo( &stInfo );
	displayPrcessorInfo(stInfo);

	GetNativeSystemInfo( &stInfo );
	displayPrcessorInfo(stInfo);
	return 0;
}