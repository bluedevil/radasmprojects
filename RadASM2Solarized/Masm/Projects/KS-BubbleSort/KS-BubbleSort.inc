; _______________________________________________________________________________
; Yordamlarin tanim dosyalari
    include windows.inc                 ;windows.inc her zaman en ustte
    include kernel32.inc 
    include masm32.inc
    include msvcrt.inc
; _______________________________________________________________________________
; Yordamlarin kendilerinin bulundugu kitapliklar
    includelib kernel32.lib 
    includelib masm32.lib
    includelib msvcrt.lib   ;olmazsa "inkey" hata veriyor

; _______________________________________________________________________________
; MASM32 makrolari
    include \masm32\macros\macros.asm   ;olmazsa "inkey" hata veriyor

; _______________________________________________________________________________
; Prototipler
    ExitProcess PROTO, dwExitCode:dword

; _______________________________________________________________________________
; Degiskenlerimiz
.data
    ; intArray adinda 10 elemanli bir dizi tanimliyoruz
    intArray BYTE 21, 43, 11, 32, 98, 23, 44, 72, 13, 91
    ileti1 BYTE "Dizimizin ilk hali: ",13,10,0
    ileti2 BYTE "Dizimizin son hali: ",13,10,0
    yeniSatir db 13, 10, 0 ;,13,10 yazdik boylece alt satira ge�mis oldu.

; _______________________________________________________________________________
; Veri icerigi belli olmayan degiskenler
.data?
    tampon01 db 12 dup(?)
    tampon02 db 12 dup(?) 

;crlf: push eax
;mov eax,0Dh ; output a CR
;call StdOut
;mov eax,0Ah ; output an LF
;call StdOut
;pop eax
;ret 
;bununla proc yap dene