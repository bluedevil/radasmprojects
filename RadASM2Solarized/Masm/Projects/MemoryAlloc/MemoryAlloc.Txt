; _______________________________________________________________________________
;|                          _.'   __.'.__  .                                     |
;|                          |P`  '-.   .-'  ?\                                   |
;|                         ."h     /.'.\    "B                                   |
;|                         (""h    '   '    "P                                   |
;|  _______ __             ?""",          .""P  ______               __ ___      |
;| |   _   |  .--.--.----- {"``""oo____oo""""P |   _  \ .-----.--.--|__|   |     |
;| |.  1   |  |  |  |  -__| '""888888888888,;  |.  |   \|  -__|  |  |  |.  |     |
;| |.  _   |__|_____|_____|  `?88P^\,?88^\,Y   |.  |    |_____|\___/|__|.  |___  |
;| |:  1    \                  88?\__d88\_/'   |:  1    / .            |:  1   | |
;| |::.. .  /                  `8o8888/\88P    |::.. . /               |::.. . | |
;| `-------'                    ,?oo88oo8P     `------'                `-------' |
;|                          ===~88~|\\\\|~====                                   |
;|                                                                               |
;|                                          oldurmeyen her darbe, guce guc katar |
;|_______________________________________________________________________________|
; Bellek Ayirma Yordamlari Uygulamasi
; _______________________________________________________________________________
; Yazar     : BlueDeviL <bluedevil@sctzine.com>
; Tester    : ErrorInside <errorinside@sctzine.com>
; IDE       : RADAssembler v2.2.2.2 <http://masm32.com/board/index.php?board=24.0>
; Taslak    : BlueDeviL // SCT
; Tarih     : 05.05.2017
; License   : MIT
; _______________________________________________________________________________
;                                                                 www.sctzine.com
;                                            oldurmeyen her darbe guce guc katar!

->  05.05.2017  MemoryAlloc Uygulamasi Taslagi

    Esenlikler! Bu ornegimizde HeapCreate ile yeni bir yigit-heap yaratabilir Heap
    Alloc ile bu yigittan bellek blogu ayirabilirsiniz. HeapCreate yerine aslinda 
    GetProcessHeap ile zaten isletim sisteminin bizim i�in yarattigi yigitin HANDLE
    degerini dondurebiliriz.
    Global ve Local bellek yordamlari zaten Heap yordamlarini sarmalayan(wrapper)
    yordamlardir.
    VirtualAlloc 32bitlik windows uygulamalarimizda kullanmamiz en uygun APIdir.
    Cagiran processin-islemin sanal adres uzayinda bize yer ayirir. Ozellikleri de
    oldukca gelismistir.
    HeapCreate
    HeapAlloc
    HeapLock
    HeapUnlock
    HeapFree
    HeapDestroy
    GlobalAlloc
    GlobalLock
    GlobalUnlock
    GlobalFree
    LocalAlloc
    LocalLock
    LocalUnlock
    LocalFree
    VirtualAlloc
    VirtualLock
    VirtualUnlock
    VirtualFree
    SetFilePointer
    GetProcessHeap
    bu ornekte isledigimiz Windows APIleridir. Bu apileri degisik seceneklerle
    uygulayarak ve debug ederek ortaya cikan sonuclari incelerseniz daha iyi kavramis olursunuz.
    BlueDeviL // SCT
