; _______________________________________________________________________________
; Yordamlarin tanim dosyalari
    include windows.inc                 ;windows.inc her zaman en ustte
    include ntdll.inc
    include masm32.inc
    include user32.inc
    include msvcrt.inc
    include advapi32.inc
    include kernel32.inc


; _______________________________________________________________________________
; MASM32 makrolari
    include \masm32\macros\macros.asm

; _______________________________________________________________________________
; Yordamlarin kendilerinin bulundugu kitapliklar
    includelib ntdll.lib
    includelib masm32.lib
    includelib user32.lib
    includelib msvcrt.lib
    includelib advapi32.lib
    includelib kernel32.lib

; _______________________________________________________________________________
; Prototipler


; _______________________________________________________________________________
; Yapilar
    RTL_OSVERSIONINFOEXW STRUCT
      dwOSVersionInfoSize   DWORD ?
      dwMajorVersion        DWORD ?
      dwMinorVersion        DWORD ?
      dwBuildNumber         DWORD ?
      dwPlatformId          DWORD ?
      szCSDVersion          WORD  128  dup (?)
      wServicePackMajor     WORD ?
      wServicePackMinor     WORD ?
      wSuiteMask            WORD ?
      wProductType          BYTE ?
      wReserved             BYTE ?
    RTL_OSVERSIONINFOEXW ENDS

; _______________________________________________________________________________
; Sabitler
.const

; _______________________________________________________________________________
; Degiskenlerimiz
.data
    sctGS               db "  ______________________________________________________________",13,10
                        db " |  _.'   __.'.__  .                                            |",13,10
                        db " |  |P`  '-.   .-'  ?\   _____ _         ____          _ __     |",13,10
                        db " | .'h     /.'.\    'B  | __  | |_ _ ___|    \ ___ _ _|_|  |    |",13,10
                        db " | (''h    '   '    'P  | __ -| | | | -_|  |  | -_| | | |  |__  |",13,10
                        db " | ?''',          .''P  |_____|_|___|___|____/|___|\_/|_|_____| |",13,10
                        db " | {'``''oo____oo''''P                                          |",13,10
                        db " |  '''888888888888,;      oldurmeyen her darbe,guce guc katar! |",13,10
                        db " |   `?88P^\,?88^\,Y                                            |",13,10
                        db " |     88?\__d88\_/'                                            |",13,10
                        db " |     `8o8888/\88P                                             |",13,10
                        db " |      ,?oo88oo8P                              www.sctzine.com |",13,10
                        db " |  ===~88~|\\\\|~====                 home of secret reversers |",13,10
                        db " |______________________________________________________________|",13,10,13,10,0
    konsolBaslik        db  "[SCT]OsVersionX Application ",0 
    szRegPath           db  "SOFTWARE\Microsoft\Windows NT\CurrentVersion",0
    szRegWin10Maj       db  "CurrentMajorVersionNumber",0
    szRegWin10Min       db  "CurrentMinorVersionNumber",0
    szRegWinCV          db  "CurrentVersion",0
    szProductName       db  "ProductName",0
    szRegKey            db  "CurrentVersion",0
    szLibraryPath       db  "msvcrt.dll",0
    szMajorVersion      db  "_get_winmajor",0
    szMinorVersion      db  "_get_winminor",0
    szWinVer            db  "_get_winver",0
    format              db  "%d",0
    formatS             db  "%hs",0
    szBufferProductN    dd  32
    szBufferMajor       dd  32
    szBufferMinor       dd  32
    dwMajorVersion      dd  0
    dwMinorVersion      dd  0
    dwWinVer            dd  0

; _______________________________________________________________________________
; Veri icerigi belli olmayan degiskenler
.data?
    genelTampon         db  32  dup (?)
    bufferProductN      db  32  dup (?)
    bufferMajor         db  32  dup (?)
    bufferMinor         db  32  dup (?)
    binBuff             db  17  dup (?)
    osv                 OSVERSIONINFO           <>
    osvx                OSVERSIONINFOEX         <>
    osvxa               OSVERSIONINFOEXA        <>
    rtlOsvx             RTL_OSVERSIONINFOEXW    <>
    hKey                dd  ?










