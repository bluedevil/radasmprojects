; _______________________________________________________________________________
; include files
    include windows.inc             ;windows.inc must always on top
    include user32.inc
    include kernel32.inc
    include comctl32.inc
    include shell32.inc
    include Lib/AniGIF.inc
    include Lib\ufmod.inc
    include gdi32.inc
; _______________________________________________________________________________
; libraries
    includelib winmm.lib            ;uFModun needs this lib
    includelib user32.lib
    includelib kernel32.lib
    includelib comctl32.lib         ;InitCommonControls
    includelib shell32.lib          ;ShellExecute
    includeLib Lib\AniGIF.lib
    includelib Lib\ufmod.lib
    includelib gdi32.lib
; _______________________________________________________________________________
; MASM32 macros
    include \masm32\macros\macros.asm

; _______________________________________________________________________________
; Prototypes
    PencereIslemi                   PROTO:HWND,:UINT,:WPARAM,:LPARAM
    ImlecYukle                      PROTO:HWND,:UINT,:WPARAM,:LPARAM
    SaydamPencere                   PROTO:DWORD,:DWORD
    SerialUret                      PROTO:DWORD

; _______________________________________________________________________________
; Public symbols:
    PUBLIC hInstance                        ;anilib uses

; _______________________________________________________________________________
; constant variables
.const
    IDD_ANAPENCERE                  equ 1000
    
    IDC_GIF                         equ	1001
    IDC_GRPADSRL                    equ 1002
    IDC_EDTAD                       equ 1003
    IDC_EDTSRL                      equ 1004
    IDC_BTNKPT                      equ 1005
    IDC_BTNHKKNDA                   equ 1006
    IDC_BTNKPYLA                    equ 1007
    IDC_BTNURET                     equ 1008
    IDC_CHKXM                       equ 1009
    IDC_STCAD                       equ 1010
    IDC_STCSERIAL                   equ 1011
    IDC_STCLNK                      equ 1012
    

    
    muzik                           equ 5000
    ikon                            equ 5001
    logoGif                         equ	5002
    
    TransparanDerece                equ 230 ;Transparency level ( max 254 )
    MAXSIZE                         equ	512

; _______________________________________________________________________________
; initialized variables
.data
    msgBaslik                       db "About",0
    msgHakkinda                     db "[SCT]Keygen Template v2.0.0.2 - SCTZine 2018",13,10,13,10
                                    db "Esen - Hello!",13,10
                                    db "This is a keygen template for my readers.",13,10
                                    db "I just made an english version, hope you like it",13,10
                                    db "Nice coding =)",13,10,13,10
                                    db "Features:",13,10
                                    db "XM music, transparency, link static and working buttons.",13,10
                                    db "Added color on mouse over to link static.",13,10
                                    db "And animated GIF as a logo.",13,10,13,10
                                    db "Author:",9,"BlueDeviL // SCT",13,10
                                    db "Tester:",9,"ErrorInside // SCT",13,10
                                    db "Date:",9,"23.09.2018",13,10
                                    db "E-Mail:",9,"bluedevil@sctzine.com",13,10,13,10
                                    db "Thanks:",13,10
                                    db "Errorinside, Infexia, Rnd0M, mrfearless, MASM32 forums",13,10
                                    db "[SCT] 2018",0
    
    TarayiciyaGit                   db "open", 0
    SCTweb                          db "http://www.sctzine.com/",0

; _______________________________________________________________________________
; uninitialized variables
.data?
    hInstance                       dd ?
    hSerial                         dd ?;handle to our serial
    hImlec                          dd ?
    hBrush                          dd ?
    hStcLink                        dd ?
    lpOrgStaticProc                 dd ?
    hWAGif                          dd ?
    hLib                            dd ?
    bFareUstundeMi                  dd ?
    szName                          db MAXSIZE	dup(?)
    szSerial                        db MAXSIZE	dup(?)
    szHash                          db MAXSIZE	dup(?)
    szHash2                         db MAXSIZE	dup(?)
    szHashMD5                       db MAXSIZE	dup(?)
    szLenH1                         db MAXSIZE	dup(?)
    szBuff1                         db MAXSIZE	dup(?)
    szBuff2                         db MAXSIZE	dup(?)
