; __________________________________
;|   __   ________ ___ _ __  _ ___  |
;| /' _/ / _/_   _|_  | |  \| | __| |
;| `._`.| \__ | |  / /| | | ' | _|  |
;| |___/ \__/ |_| |___|_|_|\__|___| |
;|                                  |
;|         home of secret reversers |
;|__________________________________|
; SetConsoleText Uygulamasi Taslagi
; _______________________________________________________________________________
; Yazar     : BlueDeviL <bluedevil@sctzine.com>
; Tester    : ErrorInside <errorinside@sctzine.com>
; IDE       : RADAssembler v2.2.2.2 <http://masm32.com/board/index.php?board=24.0>
; Taslak    : BlueDeviL // SCT
; Tarih     : 05.06.2017
; Lisans    : MIT
; _______________________________________________________________________________
;                                                                 www.sctzine.com
;                                            oldurmeyen her darbe guce guc katar!

->  05.06.2017  SetConsoleText Uygulamasi Taslagi
                
    Esenlikler! 
    Esen!
    Sikici siyah + gri komut satirindan siz de benim gibi sikildiysaniz bu ders a
    radiginiz sey: Assembly ile komut satirini renklendirmek. Bunun icin <a href=
    "https://msdn.microsoft.com/en-us/library/windows/desktop/ms686047(v=vs.85).a
    spx">SetConsoleTextAttribute</a> APIsini kullanacagiz.
 
    BOOL WINAPI SetConsoleTextAttribute(
      _In_ HANDLE hConsoleOutput,
      _In_ WORD   wAttributes
    )
    hConsoleOutput konsol ekraninin tamponunun tutamagi. Komut satiri en azindan 
    GENERIC_READ modundan acilmis olmali.
    wAttributes ile renk atamasi yapiliyor. Aslinda tam dogrusu karakter davranis
    lari ataniyor:
    
    Attribute                     Meaning
    FOREGROUND_BLUE             Text color contains blue.
    FOREGROUND_GREEN            Text color contains green.
    FOREGROUND_RED              Text color contains red.
    FOREGROUND_INTENSITY        Text color is intensified.
    BACKGROUND_BLUE             Background color contains blue.
    BACKGROUND_GREEN            Background color contains green.
    BACKGROUND_RED              Background color contains red.
    BACKGROUND_INTENSITY        Background color is intensified.
    COMMON_LVB_LEADING_BYTE     Leading byte.
    COMMON_LVB_TRAILING_BYTE    Trailing byte.
    COMMON_LVB_GRID_HORIZONTAL  Top horizontal.
    COMMON_LVB_GRID_LVERTICAL   Left vertical.
    COMMON_LVB_GRID_RVERTICAL   Right vertical.
    COMMON_LVB_REVERSE_VIDEO    Reverse foreground and background attributes.
    COMMON_LVB_UNDERSCORE       Underscore.

    Ben bir uygulama yazdim onun uzerinden anlatayim:
 
    SetConsoleTextColor proc fore:DWORD,back:DWORD

    LOCAL hStdOut:DWORD

    invoke GetStdHandle,STD_OUTPUT_HANDLE
    mov   hStdOut,eax
    mov   eax,back
    shl   eax,4
    or    eax,fore
    invoke SetConsoleTextAttribute,hStdOut,eax
    ret
   
    SetConsoleTextColor endp
    Arkadaslar GetStdHandle ile uzerinde calistigimiz konsolun tutamagini donduruyoruz
    Boylece SetConsoleTextAttribute APIsi icin gerekli olan tutamagi elde etmis oluyor
    uz geriye son bir parametre kaliyor o da renk degerimiz. Yani yukardaki kodda SetC
    onsoleTextAttribute APIsinin sonundaki eax aslinda rengi belirtiyor. Yukaridaki Wi
    ndows sabitleri ile metnin arka yuzunu ya da on yuzunu degistirebiliyoruz. Bu sabi
    tleri birbirleriyle karistirip ara renkler de elde edilebiliyor. Tabi bu sabitleri
    aslinda sayisal degerlere denk geliyor islemci seviyesinde ve ben sizin icin bu de
    gerleri cikarttim:
    0 = black
    1 = koyu mavi
    2 = koyu yesil
    3 = koyu turkuaz
    4 = koyu kizil
    5 = koyu morumsu(magenta)
    6 = kirli sari
    7 = gri -> normal metin
    8 = koyu gri
    9 = parlak mavi
    0Ah = parlak yesil
    0Bh = parlak turkuaz
    0Ch = parlak kizil
    0Dh = parlak morumsu(magenta)
    0Eh = parlak sari
    0Fh = parlak beyaz</pre>
    RadASM ile kodlama yapiyorsaniz sondaki alti karakteri benim yazdigim gibi onaltil
    ik da yazabilirsiniz, normal  onluk da yazabilirsiniz. Burada ben bu renkleri dene
    yerek adlandirdim. Belgelendirme de benim koyu yazdiklarima ya da parlak yazdiklar
    ima normal diyor olabilir. Cok takilmayin =)


    BlueDeviL // SCT
