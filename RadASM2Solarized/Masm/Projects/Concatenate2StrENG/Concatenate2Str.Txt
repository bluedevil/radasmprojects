; _____________________________________
;|   ________   ___________________    |
;|  |        | |                   |   |
;|  |   ___  | |___________     ___|   |
;|  |  |  |__|  ________   |   |       |
;|  |  |       |   __   |  |   |       |
;|  |  |_____  |  |  |  |  |   |       |
;|  |______  | |  |  |__|  |   |       |
;|   __    | | |  |        |   |       |
;|  |  |___| | |  |_____   |   |       |
;|  |________| |________|  |___|       |
;|                                     |
;|            home of secret reversers |
;|_____________________________________|
; Concatenate2Str MASM32 Sample Code
; _______________________________________________________________________________
; Yazar     : BlueDeviL <bluedevil@sctzine.com>
; Tester    : ErrorInside <errorinside@sctzine.com>
; IDE       : RADAssembler v2.2.2.2 <http://masm32.com/board/index.php?board=24.0>
; Taslak    : BlueDeviL // SCT
; Tarih     : 16.05.2017
; Lisans    : MIT
; _______________________________________________________________________________
;                                                                 www.sctzine.com
;                                            oldurmeyen her darbe guce guc  katar!

->  16.05.2017  Concatenate2Str MASM32 Sample Code

    Hello,
    We can use chr$, cat$, print, StdOut, lstrcat to concatenate strings.
    Look at the source code and the comment lines :)
    BlueDeviL // SCT
