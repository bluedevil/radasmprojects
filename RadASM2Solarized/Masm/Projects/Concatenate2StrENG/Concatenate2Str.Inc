; _______________________________________________________________________________
; include files
    include windows.inc ;windows.inc her zaman en ustte
    include masm32.inc
    include user32.inc
    include msvcrt.inc
    include kernel32.inc

; _______________________________________________________________________________
; masm32 macros
    include \masm32\macros\macros.asm

; _______________________________________________________________________________
; libraries
    includelib masm32.lib
    includelib user32.lib
    includelib msvcrt.lib
    includelib kernel32.lib

; _______________________________________________________________________________
; constant variables
.const
    MAXSIZE             equ	512

; _______________________________________________________________________________
;   initialized variables
.data
    sctGS               db "  ________________________________",13,10
                        db " |       ______    __ ______      |",13,10
                        db " |      / ____/   /  ]      |     |",13,10
                        db " |     |  \      /  /|      |     |",13,10
                        db " |     (   \__  /  / |      |     |",13,10
                        db " |      \__   |/  /  |_|  |_|     |",13,10
                        db " |      /  \  /   \_   |  |       |",13,10
                        db " |      \     \     |  |  |       |",13,10
                        db " |       \____|\____|  |__|       |",13,10
                        db " |                                |",13,10
                        db " |       home of secret reversers |",13,10
                        db " |________________________________|",13,10,13,10,0
    konsolBaslik        db "[SCT]Concatenate2Str MASM32 Sample Code ",0 
    strLine01           db "First half of my line",0
    strLine02           db ", second half of my line",0
    strNum01            db "First grasshopper jumps, ",0
    strNum02            db "second grasshopper jumps, ",0
    strNum03            db "third grasshopper jumps, ",0
    strNum04            db "fourth drinks red wine like a gentleman.",13,10,0
    hello               db "Hello my name is j00n",13,10,0

; _______________________________________________________________________________
; uninitialized variables
.data?
    myLength            DWORD   ?
    myStrContainer      db MAXSIZE dup(?)
    genelTampon         db 1024 (?)
