; _______________________________________________________________________________
;|                          _.'   __.'.__  .                                     |
;|                          |P`  '-.   .-'  ?\                                   |
;|                         ."h     /.'.\    "B                                   |
;|                         (""h    '   '    "P                                   |
;|  _______ __             ?""",          .""P  ______               __ ___      |
;| |   _   |  .--.--.----- {"``""oo____oo""""P |   _  \ .-----.--.--|__|   |     |
;| |.  1   |  |  |  |  -__| '""888888888888,;  |.  |   \|  -__|  |  |  |.  |     |
;| |.  _   |__|_____|_____|  `?88P^\,?88^\,Y   |.  |    |_____|\___/|__|.  |___  |
;| |:  1    \                  88?\__d88\_/'   |:  1    / .            |:  1   | |
;| |::.. .  /                  `8o8888/\88P    |::.. . /               |::.. . | |
;| `-------'                    ,?oo88oo8P     `------'                `-------' |
;|                          ===~88~|\\\\|~====                                   |
;|                                                                               |
;|                                          oldurmeyen her darbe, guce guc katar |
;|_______________________________________________________________________________|
; Dialog Animations 0.1
; _______________________________________________________________________________
; Author    : BlueDeviL <bluedevil@sctzine.com>
; Tester    : ErrorInside <errorinside@sctzine.com>
; IDE       : RADAssembler v2.2.2.0 <http://masm32.com/board/index.php?board=24.0>
; Template  : BlueDeviL // SCT
; Date      : 03/11/2012
; License   : MIT
; _______________________________________________________________________________
;                                                                 www.sctzine.com

.386                    ;tells to use the 80386 instruction set
.model flat, stdcall    ;tells which memory model and calling convention to use
option casemap :none    ;tells how to behave lowercase/uppercase

include AnimateDiyalog.inc

.code

start:

	invoke  GetModuleHandle,NULL
	mov     hInstance,eax
	invoke  LoadCursor,NULL,IDC_HAND;linke y�kleyece�imiz imlecin handle'�n� al
	mov     hImlec,eax				;hImlec de�i�kenine y�kle
    invoke  InitCommonControls
	invoke  DialogBoxParam,hInstance,pencere,NULL,addr PencereIslemi,NULL
	invoke  ExitProcess,0

ImlecYukle proc hWin:HWND,uMsg:UINT,wParam:WPARAM,lParam:LPARAM
; _______________________________________________________________________________
; This subclass loads IDC_HAND cursor to our static link.
; Also sets color when mouse is over the static
; Receives  :
; Returns   : 
; �������������������������������������������������������������������������������
    .if uMsg==WM_SETCURSOR
        invoke SetCursor, hImlec
    .else
        invoke GetWindowLong, hWin, GWL_USERDATA
        invoke CallWindowProc, eax, hWin, uMsg, wParam, lParam
        ret
    .endif
    
    xor eax, eax
    ret
ImlecYukle endp

PencereIslemi proc hWin:HWND,uMsg:UINT,wParam:WPARAM,lParam:LPARAM
; _______________________________________________________________________________
; An application-defined function that processes messages sent to a window. 
; Receives  : hWin, handle to our dialogdialog kutusu handle degeri
;             uMsg, message
;             wParam, Additional message-specific information.
;             lParam, Additional message-specific information.
; Returns   : The return value is the result of the message processing and depends on the message sent.
; �������������������������������������������������������������������������������
    mov eax,hWin
    mov hMain,eax
    push hMain;main window hadle, we will need this in the test window
    mov		eax,uMsg
    .if eax==WM_INITDIALOG
        invoke LoadIcon,hInstance,5001                  ;Our apps icon
        mov hIkon,eax
        invoke SendMessage,hWin,WM_SETICON,ICON_SMALL,hIkon;we add icon to the dialog
        
        invoke CheckDlgButton,hWin,1004,BST_CHECKED     ;check the radiobuttons
        invoke CheckDlgButton,hWin,1023,BST_CHECKED     ;on start
        
        invoke GetDlgItem,hWin,lblLINK
        push eax
        invoke SetWindowLong,eax,GWL_WNDPROC,ADDR ImlecYukle
        pop edx
        invoke SetWindowLong,edx,GWL_USERDATA,eax

	.elseif eax==WM_COMMAND
        mov eax,wParam
        mov edx,wParam
        shr edx,16
        
        .if dx==BN_CLICKED
            .if ax==lblLINK                             ;user clicked our link?
                invoke ShellExecute,hWin,ADDR TarayiciyaGit,ADDR SCTweb,NULL,NULL,0
            .endif
        .endif
        .if ax==btnTest                                 ;if user click "Try !" button
            invoke CloseWindow,hWin                     ;then close main window(minimize)
            
            ;Create our new test dialog (DialogBoxParam can be used)
            invoke CreateDialogParam,hInstance,testpencere,hWin,ADDR TestPenceresi,0
            ret
        .endif
        .if ax==btnKapat                                ;if user hits close button
            invoke IsDlgButtonChecked,hWin,1014         ;Slide from Right
            cmp eax,0
            jz _Sonraki
            mov ebx,AW_HIDE or AW_SLIDE or AW_HOR_NEGATIVE
            jmp _Son
            _Sonraki:
            invoke IsDlgButtonChecked,hWin,1015         ;Slide from Left
            cmp eax,0
            jz _Sonraki0
            mov ebx,AW_HIDE or AW_SLIDE or AW_HOR_POSITIVE
            jmp _Son
            _Sonraki0:
            invoke IsDlgButtonChecked,hWin,1016         ;Diagonal Right to Left
            cmp eax,0
            jz _Sonraki1
            mov ebx,AW_HIDE or AW_HOR_NEGATIVE or AW_VER_POSITIVE
            jmp _Son
            _Sonraki1:
            invoke IsDlgButtonChecked,hWin,1017         ;Diagonal Left to Right
            cmp eax,0
            jz _Sonraki2
            mov ebx,AW_HIDE or AW_HOR_POSITIVE or AW_VER_POSITIVE
            jmp _Son
            _Sonraki2:
            invoke IsDlgButtonChecked,hWin,1018         ;(Blending ya da fading)
            cmp eax,0
            jz _Sonraki3
            mov ebx,AW_HIDE or AW_BLEND
            jmp _Son
            _Sonraki3:
            invoke IsDlgButtonChecked,hWin,1019         ;Collapse to the Center
            cmp eax,0
            jz _Sonraki4
            mov ebx,AW_HIDE or AW_CENTER
            jmp _Son
            _Sonraki4:
            invoke IsDlgButtonChecked,hWin,1020         ;Botton to Top
            cmp eax,0
            jz _Sonraki5
            mov ebx,AW_HIDE or AW_VER_NEGATIVE
            jmp _Son
            _Sonraki5:
            invoke IsDlgButtonChecked,hWin,1021         ;Top to Bottom
            cmp eax,0
            jz _Sonraki6
            mov ebx,AW_HIDE or AW_VER_POSITIVE
            jmp _Son
            _Sonraki6:
            invoke IsDlgButtonChecked,hWin,1022         ;Right to Left
            cmp eax,0
            jz _Sonraki7
            mov ebx,AW_HIDE or AW_HOR_NEGATIVE
            jmp _Son
            _Sonraki7:
            mov ebx,AW_HIDE or AW_HOR_POSITIVE          ;Left to Right
            
            _Son:
            invoke AnimateWindow,hWin,600,ebx
            ;invoke ShowWindow,hMain,SW_RESTORE; to restore the main dialog
            invoke EndDialog,hWin,0                     ;Close the windows
            ret
        .endif
        .if ax==btnHakkinda                             ;if user hits ""About Button"
            invoke MessageBox,hWin,ADDR msgHakkinda,ADDR msgBaslik,MB_OK
            ret
        .endif
    .elseif eax==WM_CLOSE
        invoke EndDialog,hWin,0
    .else
        mov		eax,FALSE
        ret
    .endif
    mov		eax,TRUE
    ret

PencereIslemi endp

TestPenceresi proc hWin:HWND,uMsg:UINT,wParam:WPARAM,lParam:LPARAM

    .if uMsg==WM_INITDIALOG
        invoke LoadIcon,hInstance,5001                  ;add icon
        mov hIkon,eax
        invoke SendMessage,hWin,WM_SETICON,ICON_SMALL,hIkon
        ;while initializing we start checking radiobuttons then animate the dialog:
            invoke IsDlgButtonChecked,hMain,1013        ;Slide from Right
            cmp eax,0
            jz _Digeri
            mov ebx,AW_ACTIVATE or AW_SLIDE or AW_HOR_NEGATIVE
            jmp _Sonn
            _Digeri:
            invoke IsDlgButtonChecked,hMain,1012        ;Slide from Left
            cmp eax,0
            jz _Digeri0
            mov ebx,AW_ACTIVATE or AW_SLIDE or AW_HOR_POSITIVE
            jmp _Sonn
            _Digeri0:
            invoke IsDlgButtonChecked,hMain,1011        ;Diagonal Right to Left
            cmp eax,0
            jz _Digeri1
            mov ebx,AW_ACTIVATE or AW_HOR_NEGATIVE or AW_VER_POSITIVE
            jmp _Sonn
            _Digeri1:
            invoke IsDlgButtonChecked,hMain,1010        ;Diagonal Left to Right
            cmp eax,0
            jz _Digeri2
            mov ebx,AW_ACTIVATE or AW_HOR_POSITIVE or AW_VER_POSITIVE
            jmp _Sonn
            _Digeri2:
            invoke IsDlgButtonChecked,hMain,1009        ;(Blending ya da fading)
            cmp eax,0
            jz _Digeri3
            mov ebx,AW_ACTIVATE or AW_BLEND
            jmp _Sonn
            _Digeri3:
            invoke IsDlgButtonChecked,hMain,1008        ;Collapse to the Center
            cmp eax,0
            jz _Digeri4
            mov ebx,AW_ACTIVATE or AW_CENTER
            jmp _Sonn
            _Digeri4:
            invoke IsDlgButtonChecked,hMain,1007        ;Botton to Top
            cmp eax,0
            jz _Digeri5
            mov ebx,AW_ACTIVATE or AW_VER_NEGATIVE
            jmp _Sonn
            _Digeri5:
            invoke IsDlgButtonChecked,hMain,1006        ;Top to Bottom
            cmp eax,0
            jz _Digeri6
            mov ebx,AW_ACTIVATE or AW_VER_POSITIVE
            jmp _Sonn
            _Digeri6:
            invoke IsDlgButtonChecked,hMain,1005        ;Right to Left
            cmp eax,0
            jz _Digeri7
            mov ebx,AW_ACTIVATE or AW_HOR_NEGATIVE
            jmp _Sonn
            _Digeri7:
            mov ebx,AW_ACTIVATE or AW_HOR_POSITIVE      ;Left to Right
            
            _Sonn:
            invoke AnimateWindow,hWin,600,ebx
            invoke SetFocus,hWin
    .elseif uMsg==WM_COMMAND
        mov eax,wParam
        .if eax==btnKKapat                              ;if user hits Close button
            KapanmaKontrolu:
            invoke IsDlgButtonChecked,hMain,1014        ;Slide from Right
            cmp eax,0
            jz _Sonraki
            mov ebx,AW_HIDE or AW_SLIDE or AW_HOR_NEGATIVE
            jmp _Son
            _Sonraki:
            invoke IsDlgButtonChecked,hMain,1015        ;Slide from Left
            cmp eax,0
            jz _Sonraki0
            mov ebx,AW_HIDE or AW_SLIDE or AW_HOR_POSITIVE
            jmp _Son
            _Sonraki0:
            invoke IsDlgButtonChecked,hMain,1016        ;Diagonal Right to Left
            cmp eax,0
            jz _Sonraki1
            mov ebx,AW_HIDE or AW_HOR_NEGATIVE or AW_VER_POSITIVE
            jmp _Son
            _Sonraki1:
            invoke IsDlgButtonChecked,hMain,1017        ;Diagonal Left to Right
            cmp eax,0
            jz _Sonraki2
            mov ebx,AW_HIDE or AW_HOR_POSITIVE or AW_VER_POSITIVE
            jmp _Son
            _Sonraki2:
            invoke IsDlgButtonChecked,hMain,1018        ;(Blending ya da fading)
            cmp eax,0
            jz _Sonraki3
            mov ebx,AW_HIDE or AW_BLEND
            jmp _Son
            _Sonraki3:
            invoke IsDlgButtonChecked,hMain,1019        ;Collapse to the Center
            cmp eax,0
            jz _Sonraki4
            mov ebx,AW_HIDE or AW_CENTER
            jmp _Son
            _Sonraki4:
            invoke IsDlgButtonChecked,hMain,1020        ;Botton to Top
            cmp eax,0
            jz _Sonraki5
            mov ebx,AW_HIDE or AW_VER_NEGATIVE
            jmp _Son
            _Sonraki5:
            invoke IsDlgButtonChecked,hMain,1021        ;Top to Bottom
            cmp eax,0
            jz _Sonraki6
            mov ebx,AW_HIDE or AW_VER_POSITIVE
            jmp _Son
            _Sonraki6:
            invoke IsDlgButtonChecked,hMain,1022        ;Right to Left
            cmp eax,0
            jz _Sonraki7
            mov ebx,AW_HIDE or AW_HOR_NEGATIVE
            jmp _Son
            _Sonraki7:
            mov ebx,AW_HIDE or AW_HOR_POSITIVE          ;Left to Right
            
            _Son:
            
            invoke AnimateWindow,hWin,600,ebx
            invoke ShowWindow,hMain,SW_RESTORE          ;to restore the main dialog
            invoke EndDialog,hWin,0                     ;Close the windows
            ret
        .endif
    .elseif uMsg==WM_CLOSE
        ;we dont need to write down the same codes right ;)
        jmp KapanmaKontrolu
    .endif
    xor eax,eax
    ret

TestPenceresi endp

end start
