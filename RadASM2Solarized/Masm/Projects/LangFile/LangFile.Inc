; _______________________________________________________________________________
; yordamlarin tanim dosyalari
    ;include windows.inc             ;windows.inc her zaman en �stte
    ;include kernel32.inc
    ;include user32.inc
    include masm32rt.inc
; _______________________________________________________________________________
; Yordamlarin kendilerinin bulundugu kitapliklar
    ;includelib kernel32.lib
    ;includelib user32.lib

; _______________________________________________________________________________
; MASM32 makrolari
    ;include \masm32\macros\macros.asm
    
; _______________________________________________________________________________
; Prototipler
    PencereIslemi       PROTO :HWND,:UINT,:WPARAM,:LPARAM
    TranslationProcess  PROTO :HWND,:DWORD,:DWORD
    CheckLangFile       PROTO :HWND

; _______________________________________________________________________________
; Sabitler
.const
    IDD_DLG1            equ 1000
    STC_HEX             equ 1001
    STC_DEC             equ 1002
    BTN_CLOSE           equ 1003
    BTN_ABOUT           equ 1004
    BTN_SET             equ 1005
    CBO_LANG            equ 1006
    ikon                equ 5001
; _______________________________________________________________________________
; Degiskenlerimiz
.data
    szAboutText         db "Deneme",0
    szAboutCaption      db ".BlueDeviL",0
    LangFileName        db "Lang.ini",0
    
    szSectionConfig     db "CONFIG",0
    szKeyLang           db "LANG",0
    
    szSectionTR         db "TR",0
    szSectionEN         db "EN",0
    
    default4Lang        db "[CONFIG]",13,10
                        db "LANG=TR",13,10
                        db "0=TR",13,10
                        db "1=EN",13,10
                        db "[TR]",13,10
                        db "1001=Onaltilik",13,10
                        db "1002=Onluk",13,10
                        db "1003=Kapat",13,10
                        db "1004=Hakkinda",13,10
                        db "1005=Dil",13,10
                        db "[EN]",13,10
                        db "1001=Hexadecimal",13,10
                        db "1002=Decimal",13,10
                        db "1003=Close",13,10
                        db "1004=About",13,10
                        db "1005=Set Lang",13,10,0

; _______________________________________________________________________________
; Veri icerigi belli olmayan degiskenler
.data?
    hInstance           dd ?
    hLang               dd ?
    hCombo              dd ?
    
    lpNumberOfBytesRead dd ?
    lpNumberOfBytesWritten dd ?
    
    keyCode             dd ?
    
    buff4Lang           db 1024 dup(?)
    iniPath             db 512 dup(?)
    buffer1             db 128 dup(?)
    buffer2             db 128 dup(?)
