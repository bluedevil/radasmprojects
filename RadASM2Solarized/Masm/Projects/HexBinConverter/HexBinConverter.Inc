; _______________________________________________________________________________
; Yordamlarin tanim dosyalari
    include windows.inc             ;windows.inc her zaman en ustte
    include kernel32.inc
    include user32.inc
    include Comctl32.inc
    include shell32.inc
    include shlwapi.inc             ;StrToInt yordamlari icin

; _______________________________________________________________________________
; Yordamlarin kendilerinin bulundugu kitapliklar
    includelib kernel32.lib
    includelib user32.lib
    includelib Comctl32.lib
    includelib shell32.lib

; _______________________________________________________________________________
; Prototipler
    PencereIslemi   PROTO:HWND,:UINT,:WPARAM,:LPARAM
    EditAFKontrolu  PROTO:HWND,:UINT,:WPARAM,:LPARAM ;edtHEX icinin subclassing fonk.
    EditIkiKontrolu PROTO:DWORD,:DWORD,:DWORD,:DWORD ;edtBIN icin subclassing fonk.
    HexOlarakAl     PROTO:DWORD,:DWORD               ;edite girilen degeri hex olarak alan fonk. proto.
    Iki2OnA         PROTO:DWORD,:DWORD               ;ikilk degeri onaltilik degere cevirir
    OnA2Iki         PROTO:DWORD                      ;onaltilik degeri ikilik degere cevirir

; _______________________________________________________________________________
; Sabitler
.const
    pencere         equ 101
    edtHEX          equ 1004
    edtDEC          equ 1005
    edtBIN          equ 1006
    edtBIN2         equ 1007
    btnKAPAT        equ 1008
    btnYARDIM       equ 1009
    lblBITS         equ 1012
    sctIkon         equ 5001

; _______________________________________________________________________________
; Degiskenlerimiz
.data
    formatX         db "%08X",0                 ;edtHEX'e "00000000" icerisinde hex degerini yazdirir
    formats         db "%li",0                  ;long signed integer (�2,147,483,648 - 2,147,483,647)
    format0         db "%032u",0
    formatubits     db "%lu bit",0
    msgBaslik       db "Hakkinda",0
    msgYardim       db "Hex.Bin Converter",13,10,13,10
                    db "Onaltilik ve ikilik sayilar arasinda cevrim yapabilen basit",13,10
                    db "bir arac. Assembly ogrenirken sayi sistemlerini ve bu tabanlar",13,10
                    db "arasi gecisleri cok iyi bilmek ve anlamis olmak gerekir.",13,10
                    db "Bu amacla yazdigim ikinci kucuk uygulama. Edite girilen",13,10
                    db "onaltilik sayiyi yazmaca alan bir fonksiyon sayesinde bu rakam",13,10
                    db "uzerinde rahatlikla oynuyorum. B�ylelikle sayilari minciklamak",13,10
                    db "daha kolay oluyor.",13,10,13,10
                    db "Kaynak kodlari dikkatlice inceleyiniz.",13,10
                    db "Sorulariniz ve hata bildirimi i�in:",13,10
                    db "EPOSTA:",9,"bluedevil@sctzine.com",13,10
                    db "WEB:",9,"http://www.sctzine.com",13,10
                    db "FORUM:",9,"http://mdkgroup.com/forum",13,10,13,10
                    db "BlueDeviL",13,10
                    db "[SCT]",0

; _______________________________________________________________________________
; Veri icerigi belli olmayan degiskenler
.data?

    hInstance       dd ?
    EskiPenIslemi   dd ?

    CikanDeger      dd 8 dup(?)
    CikanDeger2     dd 8 dup(?)
    CikanDeger3     dd 8 dup(?)

    tampon1         dd 8 dup(?)
    tampon2         dd 20h dup(?)
    tampon3         dd 0Ah dup(?)
    tampon4         dd ?

    tampon1E        dd 8 dup(?)
    tampon2E        dd 0Ah dup(?)
    tampon3E        dd 0Bh dup(?)

    kntrlBAYRAK     DWORD ?
