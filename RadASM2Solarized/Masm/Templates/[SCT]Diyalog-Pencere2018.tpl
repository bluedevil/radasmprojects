Win32 App
[SCT]Pencere
[SCT]Pencere Taslagi v1.1 2018
[*BEGINPRO*]
[*BEGINDEF*]
[MakeDef]
Menu=1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0
1=4,O,$B\RC.EXE /v,1
2=3,O,$B\ML.EXE /c /coff /Cp /nologo /I"$I",2
3=5,O,$B\LINK.EXE /SUBSYSTEM:WINDOWS /RELEASE /VERSION:4.0 /LIBPATH:"$L" /OUT:"$5",3,4
4=0,0,,5
5=rsrc.obj,O,$B\CVTRES.EXE,rsrc.res
6=*.obj,O,$B\ML.EXE /c /coff /Cp /nologo /I"$I",*.asm
7=0,0,"$E\OllyDbg",5
[MakeFiles]
0=[SCT]Pencere.rap
1=[SCT]Pencere.rc
2=[SCT]Pencere.asm
3=[SCT]Pencere.obj
4=[SCT]Pencere.res
5=[SCT]Pencere.exe
6=[SCT]Pencere.def
7=[SCT]Pencere.dll
8=[SCT]Pencere.txt
9=[SCT]Pencere.lib
10=[SCT]Pencere.mak
11=[SCT]Pencere.hla
12=[SCT]Pencere.com
13=[SCT]Pencere.ocx
14=[SCT]Pencere.idl
15=[SCT]Pencere.tlb
[Resource]
1=ikon,5001,2,Res\sctSSari.ico
2=,1,8,Res\manifest.xml
[StringTable]
[Accel]
[VerInf]
Nme=VERINF1
ID=1
FV=1.0.0.0
PV=1.0.0.0
VerOS=0x00000004
VerFT=0x00000000
VerLNG=0x00000409
VerCHS=0x000004B0
ProductVersion=1.0.0
ProductName=
OriginalFilename=
LegalTrademarks=
LegalCopyright=
InternalName=
FileDescription=
FileVersion=1.0.0
CompanyName=
[Group]
Group=Assembly,Resources,Misc
1=1
2=1
3=2
4=
[*ENDDEF*]
[*BEGINTXT*]
[SCT]Pencere.Asm
; ����������������������������������������������?
; �?������������������������������������������?�?
; �?? �����������   �����������   ����������� ?�?
; �?? �����������   �����������   ����������� ?�?
; �?? ��            ���               ���     ?�?
; �?? ��            ��                ���     ?�?
; �?? �����������   ��                ���     ?�?
; �?? �����������   ��                ���     ?�?
; �??          ��   ��                ���     ?�?
; �??          ��   ���               ���     ?�?
; �?? �����������   �����������       ���     ?�?
; �?? �����������   �����������       ���     ?�?
; �??                                         ?�?
; �??                home of secret reversers�?�?
; �?������������������������������������������?�?
; [SCT] Pencere Taslagi - SCTZine 2018
; _______________________________________________________________________________
; Yazar     : BlueDeviL <bluedevil@sctzine.com>
; Tester    : ErrorInside <errorinside@sctzine.com>
; IDE       : RADAssembler v2.2.2.3 <http://masm32.com/board/index.php?board=24.0>
; Taslak    : BlueDeviL // SCT
; Tarih     : 19/09/2018
; Lisans    : MIT
; �������������������������������������������������������������������������������
;                                                                 www.sctzine.com
.586                    ;kullandigimiz opcodelarin hangi i�lemci setine bagli oldugu
.model flat, stdcall    ;32 bit hafiza modeli ve stdcall cagri modeli
option casemap :none    ;buyuk kucuk harfe duyarli


include [SCT]Pencere.Inc;degisken,sabit,kitaplik dosyalarimizin hepsi
                        ;yani tanilama dosyalarimiz burada

;programin kodlari buradan baslamakta
.code
;her assembly yazisi start: ile baslayip "end start" ile bitmek zorundad�r.
start:

invoke GetModuleHandle,NULL	;bu fonk. ile hafizaya adreslenen penceremizin
                        ;handle'ini aliyoruz 
mov hInstance,eax       ;bu handle'� hInstance de�i�kenine at�yoruz

;DialogBoxParam fonk. ise bir taslaktan(template'den) pencere yaratmamizi saglar
invoke DialogBoxParam,hInstance,pencere,NULL,addr PencereIslemi,NULL
invoke ExitProcess,0    ;islem sonlandir

PencereIslemi proc hWin:HWND,uMsg:UINT,wParam:WPARAM,lParam:LPARAM
; _______________________________________________________________________________
; Dialogumuzun ana islemi. Baslarken ne yapacagi, tuslara basilinca ne olacagini
; burada programliyoruz. Daha ayrintili bir ifade ile Dialog kutusuna g�nderilecek 
; mesajlarin islendigi yer bu yordam.
; Aldigi    : hWin, dialog kutusu handle degeri
;             uMsg, mesaj
;             wParam, mesaja �zg� ek bilgi
;             lParam, mesaja �zg� ek bilgi 
; D�nd�rd�g�: eax=TRUE hersey yolunda giderse
; �������������������������������������������������������������������������������
    mov		eax,uMsg
        .if eax==WM_INITDIALOG
        ;initdialog; yani initialize dialog
        ;bu VBdeki Form Load Olayina benzer
        ;program yuklenirken yani daha pencere gelmeden
        ;bazi seylerin yuklenmesini, hesaplanmasini, yapilmasini isterseniz
        ;onlari buraya yazin.Mesela Ikon yuklemek resim yuklemek
        ;Menu sag tus cubuklari ile oynamak duzenleme yapmak gibi
        invoke LoadIcon,hInstance,5001
        invoke SendMessage,hWin,WM_SETICON,ICON_SMALL,eax
        
    .elseif eax==WM_COMMAND
        ;Commamd yani komut
        ;dialogunuza bir cikis ya da hakkinda butonu koydunuz.
        ;ve tiklandiginda bir mesaj kutusu gostermesini istiyorsunuz ya da 
        ;programin kapanmasini; o komutlar buraya yaz�lacak.
        ;ufkumuzu a�al�m.
        ;"Generate" diye bir butonunuz var kullanici buraya tiklayinca ne olacak
        ;keygen algosunu hesaplayacak yordama atlayacak ;) gibi;)
        
    .elseif eax==WM_CLOSE
        ; dialogu kapatacak islemler burada yap�yoruz
        ; dinamik bellek ayirma yaptiysak onlari serbest birakiyoruz vs.
        invoke EndDialog,hWin,0
    .else
        mov		eax,FALSE
        ret
    .endif
    mov		eax,TRUE
    ret
    
PencereIslemi endp
;PencereIslemi proseduru yani islemi basladigi gibi endp ile bitmelidir.

;start ile baslayan kodlarimiz burada end start ile bitmelidir.
end start

[*ENDTXT*]
[*BEGINTXT*]
[SCT]Pencere.Inc
; _______________________________________________________________________________
; yordamlarin tanim dosyalari
    include windows.inc             ;windows.inc her zaman en �stte
    include kernel32.inc
    include user32.inc
; _______________________________________________________________________________
; Yordamlarin kendilerinin bulundugu kitapliklar
    includelib kernel32.lib
    includelib user32.lib

; _______________________________________________________________________________
; MASM32 makrolari
    include \masm32\macros\macros.asm
    
; _______________________________________________________________________________
; Prototipler
    PencereIslemi 		PROTO :HWND,:UINT,:WPARAM,:LPARAM

; _______________________________________________________________________________
; Public semboller:
    ;PUBLIC hInstance                        ;anilib bunu kullaniyor

; _______________________________________________________________________________
; Sabitler
.const
    pencere				equ 101
    ikon                equ 5001
; _______________________________________________________________________________
; Degiskenlerimiz
.data



; _______________________________________________________________________________
; Veri icerigi belli olmayan degiskenler
.data?
    hInstance			dd ?
[*ENDTXT*]
[*BEGINTXT*]
[SCT]Pencere.Rc
#include "Res/[SCT]PencereRes.rc"
#include "\masm32\include\resource.h"

#define pencere 101
;penceremizin adini ve ID'sini tanimliyoruz

pencere DIALOGEX 6,6,194,106
;"pencere" ismiyle tanimladigimiz diyalog'un
;x y dogrultusunu genislik ve yuksekligini
;bir de DIALOG ya da DIALOGEX oldugunu tanimliyoruz
;DialogEx Dialog'dan biraz daha fonksiyonel ;)

CAPTION "Bu Bizim Ilk Win32ASM Penceremiz"
;penceremizin basligi

FONT 8,"MS Sans Serif"
;penceremizin yazitipi

STYLE WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_VISIBLE | DS_ABSALIGN | DS_SYSMODAL | DS_SETFOREGROUND | DS_3DLOOK | DS_CENTER
;"style" adindan da anlasildigi gibi penceremizin ne stilde olacagi bilgilerini icerir.
;gorunur-gorunmez, pop-up, masaustunu ortala,sag menu ekleme gibi
;daha ayrintili bilgiyi RadASM menusunden "Yardim > Resource.hlp" dosyasindan bakiniz
;ayrica style'i yukaridaki gibi kullanmak icin en ustteki resource.h
;dosyasinin nerede oldugunu belirtmeniz gerekir.
;Ya da hic u�rasmayin RadASM bu i�i sizin icin yapabilir:
;0x10CF0800   yazarsaniz style'dan sonra da i�inizi g�recektir :)
;Cunk yukarida yazan WS_VISIBLE gibi degerlerin birer sayisal degeri var,
;bu sayisal degerleri birbirleriyle ORlayip da cikan sonucu yazabilirsiniz.
EXSTYLE 0x00000000
BEGIN
END
[*ENDTXT*]
[*ENDPRO*]
[*BEGINTXT*]
Res\[SCT]PencereRes.rc
#define ikon							5001
#define MANIFEST 						24
ikon					ICON      DISCARDABLE "Res/sctSSari.ico"
1						MANIFEST  DISCARDABLE "Res/manifest.xml"
[*ENDTXT*]
[*BEGINTXT*]
Res\manifest.xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<assembly xmlns="urn:schemas-microsoft-com:asm.v1" manifestVersion="1.0">
  <assemblyIdentity processorArchitecture="x86" version="5.1.0.0" name="keygen.exe" type="win32"/>
  <description></description>
  <dependency>
    <dependentAssembly>
      <assemblyIdentity type="win32" name="Microsoft.Windows.Common-Controls" version="6.0.0.0" processorArchitecture="x86" publicKeyToken="6595b64144ccf1df" language="*"/>
    </dependentAssembly>
  </dependency>
</assembly>
[*ENDTXT*]
[*BEGINBIN*]
Res\sctSSari.ico
0000010001001010000001000400280100001600000028000000100000002000
00000100040000000000000100000000000000000000000000000000000000D7
FF00362B00000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000
0000000000000001110111010000000101010101000000010101010100000001
0101010100000001010100010000000001010001000000011101000100000001
0001000100000001000101010000000101010101000000010101010100000001
0101110100000001010000010000000111011111110000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000
[*ENDBIN*]
