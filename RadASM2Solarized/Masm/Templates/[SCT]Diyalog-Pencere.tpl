Win32 App
[SCT]Pencere
Diyalog/Pencere Uygulamas�
Haz�rlayan: BlueDeviL // SCT
www.sct.tr.cx
[*BEGINPRO*]
[*BEGINDEF*]
[MakeDef]
Menu=1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0
1=4,O,$B\RC.EXE /v,1
2=3,O,$B\ML.EXE /c /coff /Cp /nologo /I"$I",2
3=5,O,$B\LINK.EXE /SUBSYSTEM:WINDOWS /RELEASE /VERSION:4.0 /LIBPATH:"$L" /OUT:"$5",3,4
4=0,0,,5
5=rsrc.obj,O,$B\CVTRES.EXE,rsrc.res
6=*.obj,O,$B\ML.EXE /c /coff /Cp /nologo /I"$I",*.asm
7=0,0,"$E\OllyDbg",5
[MakeFiles]
0=[SCT]Pencere.rap
1=[SCT]Pencere.rc
2=[SCT]Pencere.asm
3=[SCT]Pencere.obj
4=[SCT]Pencere.res
5=[SCT]Pencere.exe
6=[SCT]Pencere.def
7=[SCT]Pencere.dll
8=[SCT]Pencere.txt
9=[SCT]Pencere.lib
10=[SCT]Pencere.mak
11=[SCT]Pencere.hla
12=[SCT]Pencere.com
13=[SCT]Pencere.ocx
14=[SCT]Pencere.idl
15=[SCT]Pencere.tlb
[Resource]
[StringTable]
[Accel]
[VerInf]
Nme=VERINF1
ID=1
FV=1.0.0.0
PV=1.0.0.0
VerOS=0x00000004
VerFT=0x00000000
VerLNG=0x00000409
VerCHS=0x000004B0
ProductVersion=1.0.0
ProductName=
OriginalFilename=
LegalTrademarks=
LegalCopyright=
InternalName=
FileDescription=
FileVersion=1.0.0
CompanyName=
[Group]
Group=Added files,Assembly,Resources,Misc,Modules
1=2
2=2
3=3
[*ENDDEF*]
[*BEGINTXT*]
[SCT]Pencere.Asm
; ����������������������������������������������?
; �?������������������������������������������?�?
; �?? �����������   �����������   ����������� ?�?
; �?? �����������   �����������   ����������� ?�?
; �?? ��            ���               ���     ?�?
; �?? ��            ��                ���     ?�?
; �?? �����������   ��                ���     ?�?
; �?? �����������   ��                ���     ?�?
; �??          ��   ��                ���     ?�?
; �??          ��   ���               ���     ?�?
; �?? �����������   �����������       ���     ?�?
; �?? �����������   �����������       ���     ?�?
; �??                                         ?�?
; �??                home of secret reversers�?�?
; �?������������������������������������������?�?
; [SCT]Pencere for SCTZiNE #15
; _______________________________________________________________________________
; Yazar		: BlueDeviL <blau_devil@hotmail.com>
; Tester	: ErrorInside <errorinside@hotmail.com>
; IDE		: RADAssembler v2.2.0.5 <www.radasm.com>
; Taslak	: BlueDeviL // SCT
; �������������������������������������������������������������������������������
;																	www.sct.tr.cx
;											 �ld�rmeyen her darbe g�ce g�� katar!

.386					;kulland���m�z opcodelar�n hangi i�lemci setine ba�l� oldu�u
.model flat, stdcall	;32 bit haf�za modeli
option casemap :none	;b�y�k k���k harfe duyarl�

include [SCT]Pencere.Inc;de�i�ken,sabit,k�t�phane dosyalar�m�z�n hepsi
						;yani tan�mlama dosyalar�m�z burada

;program�n kodlar� buradan ba�lamakta
.code
;her assembly yaz�s� start: ile ba�lay�p end start ile betmek zorundad�r.
start:

	invoke GetModuleHandle,NULL	;bu fonk. haf�zaya adreslenen penceremizin
								;handle'�n� al�yoruz 
	mov hInstance,eax			;bu handle'� hInstance de�i�kenine at�yoruz
	
	;DialogBoxParam fonk. ise bir taslaktan(templateden) pencere yaratmam�z� sa�lar
	invoke DialogBoxParam,hInstance,pencere,NULL,addr PencereIslemi,NULL
	invoke ExitProcess,0		;i�lem sonland�r

PencereIslemi proc hWin:HWND,uMsg:UINT,wParam:WPARAM,lParam:LPARAM

	mov		eax,uMsg
	.if eax==WM_INITDIALOG
	;initdialog; yani initialize dialog
	;bu VBdeki Form Load Olay�na benzer
	;program y�klenirken yani daha pencere gelmeden
	;baz� �eylerin y�klenmesini hesaplanmas�n� yap�las�n� isterseniz
	;onlar buraya yaz�n.Mesela Ikon y�klemek resim y�klemek
	;Men� sa� tu� �ubuklar� ile oynamak d�zenleme yapmak gibi
	
	.elseif eax==WM_COMMAND
	;Commamd yani komut
	;dialogunuza bir ��k�� ya da hakk�nda butonu koydunuz.
	;ve t�kland���nda bir mesaj kutusu g�stermesini istiyorsunuz ya da 
	;program�n kapanmas�n� o komutlar buraya yaz�lacak.
	;ufkumuzu a�al�m.
	;"Generate" diye bir butonunuz var kullan�c� buraya t�klay�nca ne olacak
	;keygen algosunu hesaplayacak i�leme atlayacak ;) gibi;)

	.elseif eax==WM_CLOSE
	;close; dialogu kapat�r
		invoke EndDialog,hWin,0
	.else
		mov		eax,FALSE
		ret
	.endif
	mov		eax,TRUE
	ret

PencereIslemi endp
;Pencere��lemi prosed�r� yani i�lemi ba�lad��� gibi endp ile bitmelidir.

;start ile ba�layan kodlar�m�z burada end start ile bitmelidir.
end start

[*ENDTXT*]
[*BEGINTXT*]
[SCT]Pencere.Inc
include windows.inc
include kernel32.inc
include user32.inc

includelib kernel32.lib
includelib user32.lib


PencereIslemi 		PROTO :HWND,:UINT,:WPARAM,:LPARAM

;de�i�kenlerimiz
.data

;sabitlerimiz
.const
pencere				equ 101

;veri b�y�kl��� belli olmayan de�i�kenlerimiz
.data?
hInstance			dd ?
[*ENDTXT*]
[*BEGINTXT*]
[SCT]Pencere.Rc
#include "\masm32\include\resource.h"

#define pencere 101
;penceremizin ad�n� ve ID'sini tan�ml�yoruz

pencere DIALOGEX 6,6,194,106
;pencere ismiyle tan�mlad���m�z diyalog'un
;x y do�rultusunu geni�lik ve y�ksekli�ini
;bir de DIALOG ya da DIALOGEX oldu�unu tan�ml�yoruz
;DialogEx Dialog'dan biraz daha fonksiyonel ;)

CAPTION "Bu Bizim �lk Win32ASM Penceremiz"
;penceremizin ba�l���

FONT 8,"MS Sans Serif"
;penceremizin yaz�tipi

STYLE WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_VISIBLE | DS_ABSALIGN | DS_SYSMODAL | DS_SETFOREGROUND | DS_3DLOOK | DS_CENTER
;style ad�ndan da anla��ld��� gibi penceremizin ne stilde olca�� bilgilerini i�erir.
;g�r�n�r g�r�nmez, pop-up, masa�st�n� ortala,sa� men� ekleme gibi
;daha ayr�nt�l� bilgiyi RadASM men�s�nden "Yard�m > Resource.hlp" dosyas�ndan bak�n�z
;ayr�ca style'� yukar�daki gibi kullanmak i�in en �stteki resource.h
;dosyas�n�n nerede oldu�unu belirtmeniz gerekir.
;Ya da hi� u�ra�may�n RadASM bu i�i sizin i�in yapabilir:
;0x10CF0800   yazarsan�z style'dan sonra da i�inizi g�recektir :)
EXSTYLE 0x00000000
BEGIN
END
[*ENDTXT*]
[*ENDPRO*]
[*BEGINTXT*]
[SCT]Pencere.Asm
; ����������������������������������������������?
; �?������������������������������������������?�?
; �?? �����������   �����������   ����������� ?�?
; �?? �����������   �����������   ����������� ?�?
; �?? ��            ���               ���     ?�?
; �?? ��            ��                ���     ?�?
; �?? �����������   ��                ���     ?�?
; �?? �����������   ��                ���     ?�?
; �??          ��   ��                ���     ?�?
; �??          ��   ���               ���     ?�?
; �?? �����������   �����������       ���     ?�?
; �?? �����������   �����������       ���     ?�?
; �??                                         ?�?
; �??                home of secret reversers�?�?
; �?������������������������������������������?�?
; [SCT]Pencere for SCTZiNE #15
; _______________________________________________________________________________
; Yazar		: BlueDeviL <blau_devil@hotmail.com>
; Tester	: ErrorInside <errorinside@hotmail.com>
; IDE		: RADAssembler v2.2.0.5 <www.radasm.com>
; Taslak	: BlueDeviL // SCT
; �������������������������������������������������������������������������������
;																	www.sct.tr.cx
;											 �ld�rmeyen her darbe g�ce g�� katar!

.386					;kulland���m�z opcodelar�n hangi i�lemci setine ba�l� oldu�u
.model flat, stdcall	;32 bit haf�za modeli
option casemap :none	;b�y�k k���k harfe duyarl�

include [SCT]Pencere.Inc;de�i�ken,sabit,k�t�phane dosyalar�m�z�n hepsi
						;yani tan�mlama dosyalar�m�z burada

;program�n kodlar� buradan ba�lamakta
.code
;her assembly yaz�s� start: ile ba�lay�p end start ile betmek zorundad�r.
start:

	invoke GetModuleHandle,NULL	;bu fonk. haf�zaya adreslenen penceremizin
								;handle'�n� al�yoruz 
	mov hInstance,eax			;bu handle'� hInstance de�i�kenine at�yoruz
	
	;DialogBoxParam fonk. ise bir taslaktan(templateden) pencere yaratmam�z� sa�lar
	invoke DialogBoxParam,hInstance,pencere,NULL,addr PencereIslemi,NULL
	invoke ExitProcess,0		;i�lem sonland�r

PencereIslemi proc hWin:HWND,uMsg:UINT,wParam:WPARAM,lParam:LPARAM

	mov		eax,uMsg
	.if eax==WM_INITDIALOG
	;initdialog; yani initialize dialog
	;bu VBdeki Form Load Olay�na benzer
	;program y�klenirken yani daha pencere gelmeden
	;baz� �eylerin y�klenmesini hesaplanmas�n� yap�las�n� isterseniz
	;onlar buraya yaz�n.Mesela Ikon y�klemek resim y�klemek
	;Men� sa� tu� �ubuklar� ile oynamak d�zenleme yapmak gibi
	
	.elseif eax==WM_COMMAND
	;Commamd yani komut
	;dialogunuza bir ��k�� ya da hakk�nda butonu koydunuz.
	;ve t�kland���nda bir mesaj kutusu g�stermesini istiyorsunuz ya da 
	;program�n kapanmas�n� o komutlar buraya yaz�lacak.
	;ufkumuzu a�al�m.
	;"Generate" diye bir butonunuz var kullan�c� buraya t�klay�nca ne olacak
	;keygen algosunu hesaplayacak i�leme atlayacak ;) gibi;)

	.elseif eax==WM_CLOSE
	;close; dialogu kapat�r
		invoke EndDialog,hWin,0
	.else
		mov		eax,FALSE
		ret
	.endif
	mov		eax,TRUE
	ret

PencereIslemi endp
;Pencere��lemi prosed�r� yani i�lemi ba�lad��� gibi endp ile bitmelidir.

;start ile ba�layan kodlar�m�z burada end start ile bitmelidir.
end start

[*ENDTXT*]
[*BEGINTXT*]
[SCT]Pencere.Inc
include windows.inc
include kernel32.inc
include user32.inc

includelib kernel32.lib
includelib user32.lib


PencereIslemi 		PROTO :HWND,:UINT,:WPARAM,:LPARAM

;de�i�kenlerimiz
.data

;sabitlerimiz
.const
pencere				equ 101

;veri b�y�kl��� belli olmayan de�i�kenlerimiz
.data?
hInstance			dd ?
[*ENDTXT*]
[*BEGINTXT*]
[SCT]Pencere.Rc
#include "\masm32\include\resource.h"

#define pencere 101
;penceremizin ad�n� ve ID'sini tan�ml�yoruz

pencere DIALOGEX 6,6,194,106
;pencere ismiyle tan�mlad���m�z diyalog'un
;x y do�rultusunu geni�lik ve y�ksekli�ini
;bir de DIALOG ya da DIALOGEX oldu�unu tan�ml�yoruz
;DialogEx Dialog'dan biraz daha fonksiyonel ;)

CAPTION "Bu Bizim �lk Win32ASM Penceremiz"
;penceremizin ba�l���

FONT 8,"MS Sans Serif"
;penceremizin yaz�tipi

STYLE WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_VISIBLE | DS_ABSALIGN | DS_SYSMODAL | DS_SETFOREGROUND | DS_3DLOOK | DS_CENTER
;style ad�ndan da anla��ld��� gibi penceremizin ne stilde olca�� bilgilerini i�erir.
;g�r�n�r g�r�nmez, pop-up, masa�st�n� ortala,sa� men� ekleme gibi
;daha ayr�nt�l� bilgiyi RadASM men�s�nden "Yard�m > Resource.hlp" dosyas�ndan bak�n�z
;ayr�ca style'� yukar�daki gibi kullanmak i�in en �stteki resource.h
;dosyas�n�n nerede oldu�unu belirtmeniz gerekir.
;Ya da hi� u�ra�may�n RadASM bu i�i sizin i�in yapabilir:
;0x10CF0800   yazarsan�z style'dan sonra da i�inizi g�recektir :)
EXSTYLE 0x00000000
BEGIN
END
[*ENDTXT*]
