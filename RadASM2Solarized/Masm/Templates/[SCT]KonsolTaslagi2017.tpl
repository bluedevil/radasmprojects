Console App
[SCT]Konsol
[SCT]RadASM Konsol Uygulamasi Taslagi
[*BEGINPRO*]
[*BEGINDEF*]
[MakeDef]
Menu=0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0
1=4,O,$B\RC.EXE /v,1
2=3,O,$B\ML.EXE /c /coff /Cp /nologo /I"$I",2
3=5,O,$B\LINK.EXE /SUBSYSTEM:CONSOLE /RELEASE /VERSION:4.0 /LIBPATH:"$L" /OUT:"$5",3
4=0,0,,5
5=rsrc.obj,O,$B\CVTRES.EXE,rsrc.res
6=*.obj,O,$B\ML.EXE /c /coff /Cp /nologo /I"$I",*.asm
7=0,0,"$E\OllyDbg",5
[MakeFiles]
0=[SCT]Konsol.rap
1=[SCT]Konsol.rc
2=[SCT]Konsol.asm
3=[SCT]Konsol.obj
4=[SCT]Konsol.res
5=[SCT]Konsol.exe
6=[SCT]Konsol.def
7=[SCT]Konsol.dll
8=[SCT]Konsol.txt
9=[SCT]Konsol.lib
10=[SCT]Konsol.mak
11=[SCT]Konsol.hla
12=[SCT]Konsol.com
13=[SCT]Konsol.ocx
14=[SCT]Konsol.idl
15=[SCT]Konsol.tlb
16=[SCT]Konsol.sys
[Resource]
[StringTable]
[Accel]
[VerInf]
[Group]
Group=Assembly,Resources,Misc
1=1
2=1
3=3
[*ENDDEF*]
[*BEGINTXT*]
[SCT]Konsol.Asm
; �����������������������������������������������
; ��|�����������������������������������������|��
; ��| �����������   �����������   ����������� |��
; ��| �����������   �����������   ����������� |��
; ��| ��            ���               ���     |��
; ��| ��            ��                ���     |��
; ��| �����������   ��                ���     |��
; ��| �����������   ��                ���     |��
; ��|          ��   ��                ���     |��
; ��|          ��   ���               ���     |��
; ��| �����������   �����������       ���     |��
; ��| �����������   �����������       ���     |��
; ��|                                         |��
; ��|                home of secret reversers�|��
; ��|_________________________________________|��
; [SCT]Konsol Uygulamas� Tasla��
; _______________________________________________________________________________
; Yazar		: BlueDeviL <bluedevil@sctzine.com>
; Tester	: ErrorInside <errorinside@sctzine.com>
; IDE		: RADAssembler v2.2.2.2 <http://masm32.com/board/index.php?board=24.0>
; Taslak	: BlueDeviL // SCT
; Tarih  	: 05.05.2017
; �������������������������������������������������������������������������������
;																  www.sctzine.com
TITLE  [SCT]RadASM Konsol Uygulamasi Taslagi	 				  (main.asm)
.386					;kulland���m�z opcodelar�n hangi i�lemci setine ba�l� oldu�u
.model flat, stdcall	;32 bit haf�za modeli
option casemap :none	;b�y�k k���k harfe duyarl�


include [SCT]Konsol.Inc

.code
start:
	call main
	inkey				;��kmadan �nce bekle
	exit
	
main proc
	LOCAL tampon1:DWORD            					; DWORD boyutunda bir de�i�ken tan�mlad�m
    LOCAL tampon2:DWORD
    LOCAL string1:DWORD            					; ayn� boyutta bir de�i�ken daha buna da string-dizileri ataca��m
    LOCAL string2:DWORD
    LOCAL buffer1[512]:DWORD
    LOCAL buffer2[512]:DWORD 
	invoke SetConsoleTitle,addr konsolBaslik		;Konsolumuza ba�l�k koyal�m
	invoke ClearScreen								;ekran� temizler
	invoke locate,10,10								;imleci 10,10 koordinatlar�na kayd�r�r
	
    ;01. SdtOut ile ekrana bir ba�l�k atal�m:
	invoke StdOut, chr$("PRINT Fonksiyonu Ornekleri",13,10,13,10,"01. chr$ ile yazmak:",13,10,7)
	print chr$("chr$ ile buraya yazi yazdirdik ve  13,10 ile yeni satira gectik",13,10)
	print cfm$("\n")
	
	;02. print chr$ yaln�z kullan�labilece�i gibi; StdOut ile birlikte chr$ de kullan�labilir
	invoke StdOut, chr$("02. add$ ile iki stringi birlestirmek: degisken ile:",13,10)
	mov eax, add$(addr ileti01,addr ileti02)
	invoke StdOut, addr ileti01
	invoke StdOut, chr$(13,10,13,10) ; yeni sat�r yapt�k iki adet
	
	;03. cat$ g�zel bir fonksiyon, de�i�ken chr$ str$ beraber kullanmay� sa�l�yor:
	invoke StdOut, chr$("03. cat$ ile birle�tirme yapmak:",13,10)
	print cat$(chr$(13, 10, "Stack/BP="), str$(esp), chr$("/"), str$(ebp), chr$(13,10))
	mov tampon1,ptr$(buffer1)
	print cat$(tampon1,"Baska bir deneme","-","asdas"," - ","denemeasdasdae",chr$(13,10,13,10))
	
	;04. cfm$ ile C tarz� formatl� yaz� yazabiliyoruz:
	invoke StdOut, chr$("04. cfm$ ile kacis karakterlerini kullanmak:",13,10)
	print cfm$("\tDeneme yapalim\n\tbu \qfmt$\q macrosundan geliyor\n",13,10)
	
	;05. str$ ile tamsay�lar� stringe �evirip ekrana yazabiliriz
	invoke StdOut, chr$("05. ileti01 degiskeninin adresini str$ ile dondurelim:",9); 9 tab yapmaya yar�yor
	mov ecx, offset ileti01 ; addr burada �al��maz
	print str$(ecx);i�aretsiz 32bitlik tamsay�y� stringe d�nd�r ve ekrana yaz
	print chr$(13,10,13,10); iki adet yeni sat�r a�t�k
							;bunu ��ylede yazabiliriz
							;print chr$(0Dh,0Ah,0Dh,0Ah)
	print offset ileti01
	print chr$(13,10)
	print addr ileti01
	print chr$(13,10,13,10)
	print offset ileti02
	print chr$(13,10)
	print addr ileti02
	print chr$(13,10,13,10)
	
	
	;�imdi LOCAL de�i�kenler ile �al��al�m:
	;06. de�i�kene do�rudan bir say� atal�m (immediate) ve yazd�ral�m:
	mov tampon1,13 ; buraya onluk 13 tamsay�s�n� att�k
	print str$(tampon1)
	print chr$(13,10,13,10)
	
	;07. input("") kullanarak ekrana bir�ey yazd�r�p kullan�c�dan girdi yapmas�n� isteyebiliriz:
	mov string1, input("07. Bir string girin",9,9,":")
	print string1
	print chr$(0Dh,0Ah,0Dh, 0Ah)
	
	;07a. ltrim ile dizinin en solundaki bo�luklar temizlenir ara bo�luklar kal�r.
	mov string1, ltrim$(string1)
	print string1
	print chr$(13,10) 
	
	;07b. lcase$ k���k harflere d�nd�r�r t�rk�e karakter desteklemez
	print lcase$(string1)
	print chr$(0Dh,0Ah,0Dh, 0Ah)
	
	;07c. len ile girilen stringin uzunlu�unu ��renebiliriz, len tamsay� d�nd�r�r.
	print str$(len(string1))
	print chr$(0Dh,0Ah,0Dh, 0Ah)
	
	;07�. Kullan�c�n�n giridi�i dizinin ilk 5 karakterini yazd�ral�m:
	print left$(string1,5)
	print chr$(13,10)
	
	;08. Kullan�c�dan yeni bir dizi girmesini isteyelim:
	mov string1, input("08. Bir dizi girin 5 haneden uzun olsun",9,":")
	print string1
	print chr$(13,10)
	;08a. Kullan�c�n�n girdi�i dizinin son 5 karakterini yazd�ral�m: 
	print cat$(right$(string1,5), chr$(13,10,13,10))
	;a�a��daki iki sat�r yazd�rman�n ba�ka bir y�ntemi:
	;mov edx, len(string1)
	;print str$(edx)
	
	;09. Kullan�c�dan 32bit onalt�l�k bir�eyler yazmas�n� isteyelim:
	mov string2, input("09. Onaltilik birseyler yaz - 32 bit olsun",9,":")
	;print hval(string2) ----> runtime error verir
	;09a hval ile onluk de�erine d�nd�rebiliriz:
	print cat$(chr$("Onluk ederi :",9,": "), str$(hval(string2)), chr$(13,10))
	print str$(hval(string2))
	print chr$(13,10,13,10)
	
	;10. Kullan�c�dan tamsay� girmesini isteyelim ve girdi�i say�n�n iki kat�n� verelim:
	mov tampon2, input("10. Iki katini ogrenmek icin tamsayi girin",9,9,":")
	;10a. uval tampon2deki string olan rakamlar� tamsay�ya �evirip eax'e yazd�.
	mov eax, uval(tampon2)
	mov ecx, 2
	imul eax,ecx		;eax = eax * ecx
	print str$(eax) 
	print chr$(13,10,13,10)
	
	;11. Kullan�c�dan input alman�n ba�ka bir yolu StdIn:
	print chr$("10. Bir string daha girelim",9,9,":")
	invoke StdIn, addr genelTampon,1024
	invoke StdOut, addr genelTampon
	invoke StdOut, chr$(13,10,13,10)
	;invoke ExitProcess,0
	ret

main endp

end start
[*ENDTXT*]
[*BEGINTXT*]
[SCT]Konsol.Inc
;fonksiyon tan�m dosyalar�
include windows.inc					;windows.inc her zaman en �stte

include masm32.inc
include user32.inc
include msvcrt.inc
include kernel32.inc

;masm32 makrolar�
include \masm32\macros\macros.asm

;fonksiyonlar�n kendilerinin bulundu�u k�t�phaneler
includelib masm32.lib
includelib user32.lib
includelib msvcrt.lib
includelib kernel32.lib

;sabitler
.const

;de�i�kenlerimiz
.data
	konsolBaslik	db	"[SCT]RadASM Konsol Uygulamasi Taslagi ",0 
	ileti01			db	"Burasi ilk satir",0
	ileti02			db	", bu da ikincisi",0
	dnm01			db	"Deneme,",0
	dnm02			db	"bir,",0
	dnm03			db	"iki,",0
	dnm04			db	"uj",0

;verib�y�kl��� belli olmayan de�i�kenler
.data?
	genelTampon 	db 1024 (?)
[*ENDTXT*]
[*BEGINTXT*]
[SCT]Konsol.Txt
; �����������������������������������������������
; ��|�����������������������������������������|��
; ��| �����������   �����������   ����������� |��
; ��| �����������   �����������   ����������� |��
; ��| ��            ���               ���     |��
; ��| ��            ��                ���     |��
; ��| �����������   ��                ���     |��
; ��| �����������   ��                ���     |��
; ��|          ��   ��                ���     |��
; ��|          ��   ���               ���     |��
; ��| �����������   �����������       ���     |��
; ��| �����������   �����������       ���     |��
; ��|                                         |��
; ��|                home of secret reversers�|��
; ��|_________________________________________|��
; [SCT]Konsol Uygulamas� Tasla��
; _______________________________________________________________________________
; Yazar		: BlueDeviL <bluedevil@sctzine.com>
; Tester	: ErrorInside <errorinside@sctzine.com>
; IDE		: RADAssembler v2.2.2.2 <http://masm32.com/board/index.php?board=24.0>
; Taslak	: BlueDeviL // SCT
; Tarih  	: 05.05.2017
; �������������������������������������������������������������������������������
;																  www.sctzine.com
;											 �ld�rmeyen her darbe g�ce g�� katar!

->	05.05.2017	[SCT]Konsol Uygulamas� Tasla��
				
	Esenlikler! RadASM'nin g�zel �zelliklerinden birisi de s�kl�kla kulland���n�z
	bir kod yazma �ekliniz varsa bunun i�in bir taslak olu�turup h�zl�ca yazmak i
	stedi�iniz uygulamay� yazabilir her seferinde o ba�l�k fonksiyon tan�m dosyal
	ar� vb bilgileri tekrar tekrar yazmak zorunda kalmazs�n�z. E�er yeni ��reniyo
	rsan�z uzunca bir s�re sat�r sat�r yazman�zda bence yarar var. B�ylece assemb
	ly'yi daha iyi ��renebilirsiniz.
	BlueDeviL // SCT
[*ENDTXT*]
[*ENDPRO*]
