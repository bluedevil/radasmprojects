Win32 App
Sakatla
[SCT]Sakatla Patcher Tasla�� v1.0
Haz�rlayan: BlueDeviL // SCT
[*BEGINPRO*]
[*BEGINDEF*]
[MakeDef]
Menu=0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0
1=4,O,$B\RC.EXE /v,1
2=3,O,$B\ML.EXE /c /coff /Cp /nologo /I"$I",2
3=5,O,$B\LINK.EXE /SUBSYSTEM:WINDOWS /RELEASE /VERSION:4.0 /LIBPATH:"$L" /OUT:"$5",3
4=0,0,,5
5=rsrc.obj,O,$B\CVTRES.EXE,rsrc.res
6=*.obj,O,$B\ML.EXE /c /coff /Cp /nologo /I"$I",*.asm
7=0,0,"$E\OllyDbg",5
11=4,O,$B\RC.EXE /v,1
12=3,O,$B\ML.EXE /c /coff /Cp /Zi /nologo /I"$I",2
13=5,O,$B\LINK.EXE /SUBSYSTEM:WINDOWS /DEBUG /VERSION:4.0 /LIBPATH:"$L" /OUT:"$5",3,4
14=0,0,,5
15=rsrc.obj,O,$B\CVTRES.EXE,rsrc.res
16=*.obj,O,$B\ML.EXE /c /coff /Cp /nologo /I"$I",*.asm
17=0,0,"$E\OllyDbg",5
[MakeFiles]
0=Sakatla.rap
1=Sakatla.rc
2=Sakatla.asm
3=Sakatla.obj
4=Sakatla.res
5=Sakatla.exe
6=Sakatla.def
7=Sakatla.dll
8=Sakatla.txt
9=Sakatla.lib
10=Sakatla.mak
11=Sakatla.hla
12=Sakatla.com
13=Sakatla.ocx
14=Sakatla.idl
15=Sakatla.tlb
[Resource]
[StringTable]
[Accel]
[VerInf]
[Group]
Group=Added files,Assembly,Resources,Misc,Modules
1=2
2=2
[*ENDDEF*]
[*BEGINTXT*]
Sakatla.Asm
; ����������������������������������������������?
; �?������������������������������������������?�?
; �?? �����������   �����������   ����������� ?�?
; �?? �����������   �����������   ����������� ?�?
; �?? ��            ���               ���     ?�?
; �?? ��            ��                ���     ?�?
; �?? �����������   ��                ���     ?�?
; �?? �����������   ��                ���     ?�?
; �??          ��   ��                ���     ?�?
; �??          ��   ���               ���     ?�?
; �?? �����������   �����������       ���     ?�?
; �?? �����������   �����������       ���     ?�?
; �??                                         ?�?
; �??                home of secret reversers�?�?
; �?������������������������������������������?�?
; KafaKiran Patcher v1.0
; _______________________________________________________________________________
; Yazar		: BlueDeviL <blau_devil@hotmail.com>
; Tester	: ErrorInside <errorinside@hotmail.com>
; IDE		: RADAssembler v2.2.0.5 <www.radasm.com>
; Taslak	: BlueDeviL // SCT
; �������������������������������������������������������������������������������
;																	www.sct.tr.cx
.386
.model flat, stdcall
option casemap :none

include Sakatla.inc

.code
start:
;CreateFile Fonk. ile Hedef dosyam�z�n Handle'�n� al�yoruz ve hDosya'ya yazd�r�yoruz
invoke CreateFile, addr DosyaAdi, GENERIC_READ+GENERIC_WRITE, FILE_SHARE_READ,NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL
mov hDosya, eax
inc eax
test eax,eax
jnz _BoyutuHesapla			;e�er dosyay� a�may� ba�ar�r�sak BoyutuHesapla 'ya dallan
invoke MessageBox,NULL,ADDR hataACMA,ADDR hataBASLIK,MB_OK;yoksa hata mesaj�n� g�ster
jmp _Son					;program� kapatmak i�in sona z�pla


_BoyutuHesapla:
;GetFileSize apisi ile handele�m� �nceden ald���m�z hedef dosyan�n boyutunu hesapl�yoruz
invoke GetFileSize,hDosya,0
;d�nen de�er bayt olarak eax'e yaz�l�yor, bizde onu de�i�kene yaz�yoruz
;Haf�zaya dosya boyutu kadar yer ay�raca��m�z i�in de�eri bu de�i�kene atad�m 
mov HafizaBoyutu,eax
inc eax
;e�er dosya boyutu hesaplanabildiyse dallan
jne _Hafiza
;yoksa hata mesaj� ver ve sonlan
invoke MessageBox,NULL,ADDR hataBOYUT,ADDR hataBASLIK,MB_OK
jmp _Son

_Hafiza:
;VirtualAlloc fonk ile haf�zada hedef dosyan�n boyutu kadar bir yer a��yoruz
invoke VirtualAlloc,NULL,HafizaBoyutu,MEM_COMMIT,PAGE_READWRITE
mov hHafiza, eax
test eax,eax
;e�er o kadar yer a�abilirsek dosyay� okuyaca��z
jnz _DosyaOku
;VirtualAlloc ba�ar�s�z olursa hata mesaj� g�sterecek ve sonlanacak
invoke MessageBox,NULL,ADDR hataHAFIZA,ADDR hataBASLIK,MB_OK
jmp _Son1


_DosyaOku:
;ReadFile ile handle�n� ald���m�z hedef dosyay� �nceden haf�zada onun
;boyutu kadar ay�rd���m�z yere yaz�yoruz(yani okuyoruz)
invoke ReadFile, hDosya, hHafiza, HafizaBoyutu, addr baytOku, NULL
test eax,eax
;dosya okunma ba�ar�l� ise kontrol i�in dallan
jne _DosyaOkundu
;ancak okunma ba�ar�s�z ise hata mesaj� ver ve sonlan
invoke MessageBox,NULL,ADDR hataOKUMA,ADDR hataBASLIK,MB_OK
jmp _Son1


_DosyaOkundu:

mov ecx,fark				;de�i�en ka� bayt var? ona g�re d�ng� yapaca��z
xor ebx,ebx					;kendimize bir saya� yazma� al�yoruz
							;onun i�in ebx'i 0lad�k
_Kontrol1:
mov edi, hHafiza			;edi=dosyay� okumak i�in haf�zada a��t���m�z yerin adresi
mov edx, [yamaADRES+ebx*4]	;edx=yamalanacak yerin adresi(buray� d�ng�ye ba�lad�k.
							;b�ylece t�m adresler s�ra ile kontrol edilecek)
add edi,edx					;edi bizim yaratt���m�z haf�za adresiydi buraya
							;kontrol edece�imiz ilk haf�za adresini ekliyoruz
lea esi,yamaORIJINAL		;esi=yamalanan orijinal de�erlerin adresi
mov al,byte ptr[esi+ebx]	;al=dongu boyunca birer birer orijinal baytlar� ta��

cmp al,byte ptr[edi]		;al'deki de�er ile hedef dosyadan ald���m�z de�er birbiri ile ayn�m�?
jne _DosyaBulunamadi		;dosya bulunamad�ysa dallan
inc ebx						;ebx de�erini bir art�r
dec ecx						;ecx de�erini bir azalt
je _YazmaKontrolu			;e�er t�m de�erler okunup orjinal dosyan�n baytlar� ile kontrol edildise dallan
							;de�ilse devam
jmp _Kontrol1

_YazmaKontrolu:
mov ebx,sayac
mov edx,[yamaADRES+ebx*4]				;yama adresini edx'e dword olarak ta��yoruz

invoke SetFilePointer,hDosya,edx,0,0	;bu fonk ile a�t���m�z dosyan�n i�ine gitmek istedi�imiz
										;adrese gidiyoruz, yani de�i�ecek olan yere

inc eax
je _YazmaHatasi							;SetFilePointer ba�ar�s�z lursa hata ver yoksa devam et
lea esi,yamaDEGISEN						;yama verilerinin adresini esiye yazd�k

add esi,sayac							;esi ile sayac de�erimiz ka� ise toplad�k
;WriteFile ile de�i�ecek baytlar� tek tek yaz�yoruz
invoke WriteFile,hDosya,esi,1,offset baytYaz,NULL
test eax,eax
;yazarken bir hata ile kar��la��nca hata ver yoksa devam
je _YazmaHatasi
inc sayac
mov ebx,sayac
sub ebx,fark
jne _YazmaKontrolu

;buraya kadar sorunsuz gelindiyse YAMALANDI mesaj�n� patlat :D
invoke MessageBox,NULL,ADDR hzrMETIN,ADDR hzrBASLIK,MB_OK
jmp _Son1



_YazmaHatasi:
invoke MessageBox,NULL,ADDR hataYAZMA,ADDR hataBASLIK,MB_OK
jmp _Son1

_DosyaBulunamadi:
invoke MessageBox,NULL,ADDR hataBAYT,ADDR hataBASLIK,MB_OK

_Son1:
;E�er VirtualFree ile haf�zada her ay�rd�ysak program�n sonunda oray� kapat�yoruz
invoke VirtualFree,NULL,hHafiza,MEM_DECOMMIT

_Son:
;Tan�mlad���m�z dosyam�z�n handle'�n� kapat�yoruz
invoke CloseHandle, hDosya
;ExitProcess ile program� sonland�r�youz
invoke ExitProcess, NULL
end start
[*ENDTXT*]
[*BEGINTXT*]
Sakatla.Inc
;bunlar gerekli i�erik dosyalar�m�z
include \masm32\include\windows.inc
include \masm32\include\kernel32.inc
include \masm32\include\user32.inc

;i�erik dosyalar�m�z�n tan�mlad��� fonksiyonlar�n bulundu�u k�t�phaneler
includelib \masm32\lib\kernel32.lib
includelib \masm32\lib\user32.lib



.data
DosyaAdi 		db "filename.exe", 0		;Yamalanacak Dosyan�n ad�
;mesaj kutular�m�z�n ba�l�k ve i�erikleri
hataBASLIK		db "[SCT]Hata",0
hataACMA		db "Dosya Bulunamad�!",0
hataBOYUT		db "Dosya boyutu al�namad� - �ptal Ediliyor..",0
hataHAFIZA		db "Yetersiz Haf�za",0
hataOKUMA		db "Dosyay� Okuyam�yorum",0
hataBAYT		db "Dosya e�le�tirilemiyor yada zaten yamalanm��!",0
hataYAZMA		db "Dosyaya yazam�yorum",0

hzrMETIN		db "Dosya Sakatland�",0
hzrBASLIK		db "[SCT]Tebrikler",0

;_____________________________YAMA VER�LER�_____________________________
;a�a��daki yama verilerini de�i�tirerek yamalamak istedi�iniz dosyay�
;sakatlayabilirsiniz.(anlamayanlar i�in patchleyebilirsiniz)
;_______________________orijinal dosyan�n baytlar�______________________
yamaORIJINAL	db 062h,06Ch,075h,065h,020h

;____________________________de�i�en baytlar____________________________
yamaDEGISEN		db 073h,065h,06Ch,061h,06Dh

;________________De�i�en baytlar�n offsetleri(adresleri)________________
yamaADRES		dd 000000800h,000000801h,000000802h,000000803h,000000804h

sayac			dd 0

.data?
hDosya 			HANDLE ?		;yamalanacak dosyan�n handle de�eri i�in de�i�lkenimiz
hHafiza 		HANDLE ?		;yamalanacak dosyay� haf�zaya a�aca��m�z i�in haf�zada ay�raca��m�z�n yerin adres de�eri
baytOku 		dd ?			;ReadFile apisi okudu�u baytlar� saymak i�in bir adres de�eri istiyor onun i�in bu de�i�keni atad�k
baytYaz			dd ?			;WriteFile Apisi yazd��� baytlar� saymak i�in bir adress de�eri �a��r�yor bu de�i�ken de onun i�in
HafizaBoyutu	dd ?			;hede dosyan�n boyutunu bayt cinsinden hesaplan�p yaz�laca�� de�i�ken


.const
;hedef dosyada ka� bayt de�i�ece�ini hesaplay�p fark de�i�kenine at�yor:
fark			equ yamaDEGISEN-yamaORIJINAL
[*ENDTXT*]
[*ENDPRO*]
[*BEGINTXT*]
Sakatla.Asm
; ����������������������������������������������?
; �?������������������������������������������?�?
; �?? �����������   �����������   ����������� ?�?
; �?? �����������   �����������   ����������� ?�?
; �?? ��            ���               ���     ?�?
; �?? ��            ��                ���     ?�?
; �?? �����������   ��                ���     ?�?
; �?? �����������   ��                ���     ?�?
; �??          ��   ��                ���     ?�?
; �??          ��   ���               ���     ?�?
; �?? �����������   �����������       ���     ?�?
; �?? �����������   �����������       ���     ?�?
; �??                                         ?�?
; �??                home of secret reversers�?�?
; �?������������������������������������������?�?
; KafaKiran Patcher v1.0
; _______________________________________________________________________________
; Yazar		: BlueDeviL <blau_devil@hotmail.com>
; Tester	: ErrorInside <errorinside@hotmail.com>
; IDE		: RADAssembler v2.2.0.5 <www.radasm.com>
; Taslak	: BlueDeviL // SCT
; �������������������������������������������������������������������������������
;																	www.sct.tr.cx
.386
.model flat, stdcall
option casemap :none

include Sakatla.inc

.code
start:
;CreateFile Fonk. ile Hedef dosyam�z�n Handle'�n� al�yoruz ve hDosya'ya yazd�r�yoruz
invoke CreateFile, addr DosyaAdi, GENERIC_READ+GENERIC_WRITE, FILE_SHARE_READ,NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL
mov hDosya, eax
inc eax
test eax,eax
jnz _BoyutuHesapla			;e�er dosyay� a�may� ba�ar�r�sak BoyutuHesapla 'ya dallan
invoke MessageBox,NULL,ADDR hataACMA,ADDR hataBASLIK,MB_OK;yoksa hata mesaj�n� g�ster
jmp _Son					;program� kapatmak i�in sona z�pla


_BoyutuHesapla:
;GetFileSize apisi ile handele�m� �nceden ald���m�z hedef dosyan�n boyutunu hesapl�yoruz
invoke GetFileSize,hDosya,0
;d�nen de�er bayt olarak eax'e yaz�l�yor, bizde onu de�i�kene yaz�yoruz
;Haf�zaya dosya boyutu kadar yer ay�raca��m�z i�in de�eri bu de�i�kene atad�m 
mov HafizaBoyutu,eax
inc eax
;e�er dosya boyutu hesaplanabildiyse dallan
jne _Hafiza
;yoksa hata mesaj� ver ve sonlan
invoke MessageBox,NULL,ADDR hataBOYUT,ADDR hataBASLIK,MB_OK
jmp _Son

_Hafiza:
;VirtualAlloc fonk ile haf�zada hedef dosyan�n boyutu kadar bir yer a��yoruz
invoke VirtualAlloc,NULL,HafizaBoyutu,MEM_COMMIT,PAGE_READWRITE
mov hHafiza, eax
test eax,eax
;e�er o kadar yer a�abilirsek dosyay� okuyaca��z
jnz _DosyaOku
;VirtualAlloc ba�ar�s�z olursa hata mesaj� g�sterecek ve sonlanacak
invoke MessageBox,NULL,ADDR hataHAFIZA,ADDR hataBASLIK,MB_OK
jmp _Son1


_DosyaOku:
;ReadFile ile handle�n� ald���m�z hedef dosyay� �nceden haf�zada onun
;boyutu kadar ay�rd���m�z yere yaz�yoruz(yani okuyoruz)
invoke ReadFile, hDosya, hHafiza, HafizaBoyutu, addr baytOku, NULL
test eax,eax
;dosya okunma ba�ar�l� ise kontrol i�in dallan
jne _DosyaOkundu
;ancak okunma ba�ar�s�z ise hata mesaj� ver ve sonlan
invoke MessageBox,NULL,ADDR hataOKUMA,ADDR hataBASLIK,MB_OK
jmp _Son1


_DosyaOkundu:

mov ecx,fark				;de�i�en ka� bayt var? ona g�re d�ng� yapaca��z
xor ebx,ebx					;kendimize bir saya� yazma� al�yoruz
							;onun i�in ebx'i 0lad�k
_Kontrol1:
mov edi, hHafiza			;edi=dosyay� okumak i�in haf�zada a��t���m�z yerin adresi
mov edx, [yamaADRES+ebx*4]	;edx=yamalanacak yerin adresi(buray� d�ng�ye ba�lad�k.
							;b�ylece t�m adresler s�ra ile kontrol edilecek)
add edi,edx					;edi bizim yaratt���m�z haf�za adresiydi buraya
							;kontrol edece�imiz ilk haf�za adresini ekliyoruz
lea esi,yamaORIJINAL		;esi=yamalanan orijinal de�erlerin adresi
mov al,byte ptr[esi+ebx]	;al=dongu boyunca birer birer orijinal baytlar� ta��

cmp al,byte ptr[edi]		;al'deki de�er ile hedef dosyadan ald���m�z de�er birbiri ile ayn�m�?
jne _DosyaBulunamadi		;dosya bulunamad�ysa dallan
inc ebx						;ebx de�erini bir art�r
dec ecx						;ecx de�erini bir azalt
je _YazmaKontrolu			;e�er t�m de�erler okunup orjinal dosyan�n baytlar� ile kontrol edildise dallan
							;de�ilse devam
jmp _Kontrol1

_YazmaKontrolu:
mov ebx,sayac
mov edx,[yamaADRES+ebx*4]				;yama adresini edx'e dword olarak ta��yoruz

invoke SetFilePointer,hDosya,edx,0,0	;bu fonk ile a�t���m�z dosyan�n i�ine gitmek istedi�imiz
										;adrese gidiyoruz, yani de�i�ecek olan yere

inc eax
je _YazmaHatasi							;SetFilePointer ba�ar�s�z lursa hata ver yoksa devam et
lea esi,yamaDEGISEN						;yama verilerinin adresini esiye yazd�k

add esi,sayac							;esi ile sayac de�erimiz ka� ise toplad�k
;WriteFile ile de�i�ecek baytlar� tek tek yaz�yoruz
invoke WriteFile,hDosya,esi,1,offset baytYaz,NULL
test eax,eax
;yazarken bir hata ile kar��la��nca hata ver yoksa devam
je _YazmaHatasi
inc sayac
mov ebx,sayac
sub ebx,fark
jne _YazmaKontrolu

;buraya kadar sorunsuz gelindiyse YAMALANDI mesaj�n� patlat :D
invoke MessageBox,NULL,ADDR hzrMETIN,ADDR hzrBASLIK,MB_OK
jmp _Son1



_YazmaHatasi:
invoke MessageBox,NULL,ADDR hataYAZMA,ADDR hataBASLIK,MB_OK
jmp _Son1

_DosyaBulunamadi:
invoke MessageBox,NULL,ADDR hataBAYT,ADDR hataBASLIK,MB_OK

_Son1:
;E�er VirtualFree ile haf�zada her ay�rd�ysak program�n sonunda oray� kapat�yoruz
invoke VirtualFree,NULL,hHafiza,MEM_DECOMMIT

_Son:
;Tan�mlad���m�z dosyam�z�n handle'�n� kapat�yoruz
invoke CloseHandle, hDosya
;ExitProcess ile program� sonland�r�youz
invoke ExitProcess, NULL
end start
[*ENDTXT*]
[*BEGINTXT*]
Sakatla.Inc
;bunlar gerekli i�erik dosyalar�m�z
include \masm32\include\windows.inc
include \masm32\include\kernel32.inc
include \masm32\include\user32.inc

;i�erik dosyalar�m�z�n tan�mlad��� fonksiyonlar�n bulundu�u k�t�phaneler
includelib \masm32\lib\kernel32.lib
includelib \masm32\lib\user32.lib



.data
DosyaAdi 		db "filename.exe", 0		;Yamalanacak Dosyan�n ad�
;mesaj kutular�m�z�n ba�l�k ve i�erikleri
hataBASLIK		db "[SCT]Hata",0
hataACMA		db "Dosya Bulunamad�!",0
hataBOYUT		db "Dosya boyutu al�namad� - �ptal Ediliyor..",0
hataHAFIZA		db "Yetersiz Haf�za",0
hataOKUMA		db "Dosyay� Okuyam�yorum",0
hataBAYT		db "Dosya e�le�tirilemiyor yada zaten yamalanm��!",0
hataYAZMA		db "Dosyaya yazam�yorum",0

hzrMETIN		db "Dosya Sakatland�",0
hzrBASLIK		db "[SCT]Tebrikler",0

;_____________________________YAMA VER�LER�_____________________________
;a�a��daki yama verilerini de�i�tirerek yamalamak istedi�iniz dosyay�
;sakatlayabilirsiniz.(anlamayanlar i�in patchleyebilirsiniz)
;_______________________orijinal dosyan�n baytlar�______________________
yamaORIJINAL	db 062h,06Ch,075h,065h,020h

;____________________________de�i�en baytlar____________________________
yamaDEGISEN		db 073h,065h,06Ch,061h,06Dh

;________________De�i�en baytlar�n offsetleri(adresleri)________________
yamaADRES		dd 000000800h,000000801h,000000802h,000000803h,000000804h

sayac			dd 0

.data?
hDosya 			HANDLE ?		;yamalanacak dosyan�n handle de�eri i�in de�i�lkenimiz
hHafiza 		HANDLE ?		;yamalanacak dosyay� haf�zaya a�aca��m�z i�in haf�zada ay�raca��m�z�n yerin adres de�eri
baytOku 		dd ?			;ReadFile apisi okudu�u baytlar� saymak i�in bir adres de�eri istiyor onun i�in bu de�i�keni atad�k
baytYaz			dd ?			;WriteFile Apisi yazd��� baytlar� saymak i�in bir adress de�eri �a��r�yor bu de�i�ken de onun i�in
HafizaBoyutu	dd ?			;hede dosyan�n boyutunu bayt cinsinden hesaplan�p yaz�laca�� de�i�ken


.const
;hedef dosyada ka� bayt de�i�ece�ini hesaplay�p fark de�i�kenine at�yor:
fark			equ yamaDEGISEN-yamaORIJINAL
[*ENDTXT*]
